﻿namespace LIS_Monitor;

interface

uses
  Essy.FTDIWrapper;


type
  ConsoleApp = class
  private
    class var ftdiDevice: FTDI_Device;
    class method readFromPort(); async;
    const STX: Char = #2;
    const ETX: Char = #3;
    const ETB: Char = #23;
    const ENQ: Char = #5;
    const ACK: Char = #6;
    const NAK: Char = #21;
    const EOT: Char = #4;
    const CR: Char = #13;
    const LF: Char = #10;
  public
    class method Main(args: array of String);
  end;

implementation

uses 
  System.IO, 
  System.Threading;

class method ConsoleApp.Main(args: array of String);
begin
  var ftdiList := FTDI_DeviceInfo.EnumerateDevices('ThunderBolt B');
  if ftdiList.Count > 0 then
  begin
    ftdiDevice := new FTDI_Device(ftdiList[0]);
    ftdiDevice.SetBaudrate(9600);
    var dataBits := DataLength.EightBits;
    var par := Parity.None;
    var sb := StopBits.OneStopBit;
    ftdiDevice.SetParameters(dataBits, par, sb);
    readFromPort(); //start read thread
    ftdiDevice.EnableWaitForDataEvent();
    Console.Write('Press ENTER to send ENQ to LIS');
    Console.ReadLine();
    Console.Clear();
    Console.WriteLine('Received Data will show below (Press ENTER to stop program)');
    Console.WriteLine();
    Console.WriteLine(DateTime.Now.Ticks.ToString() + " Before ENQ");
    ftdiDevice.WriteBytes([Byte(ENQ)]);
    Console.WriteLine(DateTime.Now.Ticks.ToString() + " After ENQ");
    Thread.Sleep(100);
    Console.ReadLine();
  end
  else
  begin
    Console.WriteLine('No device detected.');
    Console.WriteLine();
    Console.Write('Press ENTER to exit.');
    Console.ReadLine();
  end;
end;

class method ConsoleApp.readFromPort();
begin
  loop
  begin
    ftdiDevice.WaitForData();
    while ftdiDevice.NumberOfBytesInReceiveBuffer > 0 do
    begin
      Console.Write(DateTime.Now.Ticks.ToString() + " ");
      var input := ftdiDevice.ReadBytes(1);
      var c := Char(input[0]);
      case c of
        ACK: Console.Write('<ACK>');
        NAK: Console.Write('<NAK>');
        ENQ: Console.Write('<ENQ>');
        ETX: Console.Write('<ETX>');
        STX: Console.Write('<STX>');
        ETB: Console.Write('<ETB>');
        EOT: Console.Write('<EOT>');
        CR: Console.Write('<CR>');
        LF: Console.WriteLine('<LF>');
      else
        Console.Write(c);
      end;
    end;
  end;
end;

end.
