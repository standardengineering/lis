﻿namespace RS232LisTester;

interface

uses
  System.Drawing,
  System.Collections,
  System.Collections.Generic,
  System.Windows.Forms,
  System.IO.Ports,
  System.ComponentModel;

type
  /// <summary>
  /// Summary description for MainForm.
  /// </summary>
  MainForm = partial class(System.Windows.Forms.Form)
  private
    method AppendText(box: RichTextBox; color: Color; text: System.String);
    method serialPort1_DataReceived(sender: System.Object; e: System.IO.Ports.SerialDataReceivedEventArgs);
    const STX: Char = #2;
    const ETX: Char = #3;
    const ETB: Char = #23;
    const ENQ: Char = #5;
    const ACK: Char = #6;
    const NAK: Char = #21;
    const EOT: Char = #4;
    const CR: Char = #13;
    const LF: Char = #10;
    method button1_Click(sender: System.Object; e: System.EventArgs);
    method MainForm_Load(sender: System.Object; e: System.EventArgs);
    method button2_Click(sender: System.Object; e: System.EventArgs);
    method button3_Click(sender: System.Object; e: System.EventArgs);
    method button4_Click(sender: System.Object; e: System.EventArgs);
  protected
    method Dispose(disposing: Boolean); override;
  public
    constructor;
  end;

implementation

{$REGION Construction and Disposition}
constructor MainForm;
begin
  //
  // Required for Windows Form Designer support
  //
  InitializeComponent();

  //
  // TODO: Add any constructor code after InitializeComponent call
  //
end;

method MainForm.Dispose(disposing: Boolean);
begin
  if disposing then begin
    if assigned(components) then
      components.Dispose();

    //
    // TODO: Add custom disposition code here
    //
  end;
  inherited Dispose(disposing);
end;
{$ENDREGION}

method MainForm.serialPort1_DataReceived(sender: System.Object; e: System.IO.Ports.SerialDataReceivedEventArgs);
begin
  if self.richTextBox1.InvokeRequired then
  begin
    self.richTextBox1.Invoke(method();
    begin
      if e.EventType = SerialData.Chars then
      begin
        var text := serialPort1.ReadExisting();
        for each c in text do
        begin
          case c of
            ACK: AppendText(richTextBox1, Color.Red, '<ACK>');
            NAK: AppendText(richTextBox1, Color.Red, '<NAK>');
            ENQ: AppendText(richTextBox1, Color.Red, '<ENQ>' + Environment.NewLine);
            LF: AppendText(richTextBox1, Color.Red, '<LF>');
            CR: AppendText(richTextBox1, Color.Red, '<CR>');
            STX: AppendText(richTextBox1, Color.Red, '<STX>');
            ETX: AppendText(richTextBox1, Color.Red, '<ETX>');
            ETB: AppendText(richTextBox1, Color.Red, '<ETB>');
            EOT: AppendText(richTextBox1, Color.Red, '<EOT>');
          else
            AppendText(richTextBox1, Color.Black, c);
          end; // case
        end;
      end;
    end);
  end;
end;

method MainForm.button1_Click(sender: System.Object; e: System.EventArgs);
begin
  serialPort1.Write(ENQ);
  AppendText(richTextBox1, Color.Blue, '<ENQ>' + Environment.NewLine);
end;

method MainForm.MainForm_Load(sender: System.Object; e: System.EventArgs);
begin
  serialPort1.Open();
end;

method MainForm.AppendText(box: RichTextBox; color: Color; text: System.String);
begin
  var start := box.TextLength;
  box.AppendText(text);
  box.ScrollToCaret();
  var &end := box.TextLength;
  box.Select(start, &end - start); 
  box.SelectionColor := color;
  box.SelectionLength := 0;
end;

method MainForm.button2_Click(sender: System.Object; e: System.EventArgs);
begin
  serialPort1.Write(ACK);
  AppendText(richTextBox1, Color.Blue, '<ACK>' + Environment.NewLine);
end;

method MainForm.button3_Click(sender: System.Object; e: System.EventArgs);
begin
  serialPort1.Write(NAK);
  AppendText(richTextBox1, Color.Blue, '<NAK>' + Environment.NewLine);
end;

method MainForm.button4_Click(sender: System.Object; e: System.EventArgs);
begin
  richTextBox1.Clear();
end;

end.
