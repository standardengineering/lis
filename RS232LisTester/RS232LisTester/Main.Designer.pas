﻿namespace RS232LisTester;

interface

uses
  System.Windows.Forms,
  System.Drawing;

type
  MainForm = partial class
  {$REGION Windows Form Designer generated fields}
  private
    components: System.ComponentModel.IContainer;
    serialPort1: System.IO.Ports.SerialPort;
    button1: System.Windows.Forms.Button;
    richTextBox1: System.Windows.Forms.RichTextBox;
    button3: System.Windows.Forms.Button;
    button2: System.Windows.Forms.Button;
    button4: System.Windows.Forms.Button;
    method InitializeComponent;
  {$ENDREGION}
  end;

implementation

{$REGION Windows Form Designer generated code}
method MainForm.InitializeComponent;
begin
  self.components := new System.ComponentModel.Container();
  var resources: System.ComponentModel.ComponentResourceManager := new System.ComponentModel.ComponentResourceManager(typeOf(MainForm));
  self.button1 := new System.Windows.Forms.Button();
  self.serialPort1 := new System.IO.Ports.SerialPort(self.components);
  self.richTextBox1 := new System.Windows.Forms.RichTextBox();
  self.button2 := new System.Windows.Forms.Button();
  self.button3 := new System.Windows.Forms.Button();
  self.button4 := new System.Windows.Forms.Button();
  self.SuspendLayout();
  // 
  // button1
  // 
  self.button1.Location := new System.Drawing.Point(12, 12);
  self.button1.Name := 'button1';
  self.button1.Size := new System.Drawing.Size(123, 52);
  self.button1.TabIndex := 0;
  self.button1.Text := 'send ENQ';
  self.button1.UseVisualStyleBackColor := true;
  self.button1.Click += new System.EventHandler(@self.button1_Click);
  // 
  // serialPort1
  // 
  self.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(@self.serialPort1_DataReceived);
  // 
  // richTextBox1
  // 
  self.richTextBox1.DetectUrls := false;
  self.richTextBox1.Location := new System.Drawing.Point(12, 70);
  self.richTextBox1.Name := 'richTextBox1';
  self.richTextBox1.ReadOnly := true;
  self.richTextBox1.ScrollBars := System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
  self.richTextBox1.Size := new System.Drawing.Size(992, 579);
  self.richTextBox1.TabIndex := 2;
  self.richTextBox1.TabStop := false;
  self.richTextBox1.Text := '';
  self.richTextBox1.WordWrap := false;
  // 
  // button2
  // 
  self.button2.Location := new System.Drawing.Point(141, 12);
  self.button2.Name := 'button2';
  self.button2.Size := new System.Drawing.Size(123, 52);
  self.button2.TabIndex := 3;
  self.button2.Text := 'send ACK';
  self.button2.UseVisualStyleBackColor := true;
  self.button2.Click += new System.EventHandler(@self.button2_Click);
  // 
  // button3
  // 
  self.button3.Location := new System.Drawing.Point(270, 12);
  self.button3.Name := 'button3';
  self.button3.Size := new System.Drawing.Size(123, 52);
  self.button3.TabIndex := 4;
  self.button3.Text := 'send NAK';
  self.button3.UseVisualStyleBackColor := true;
  self.button3.Click += new System.EventHandler(@self.button3_Click);
  // 
  // button4
  // 
  self.button4.Location := new System.Drawing.Point(881, 12);
  self.button4.Name := 'button4';
  self.button4.Size := new System.Drawing.Size(123, 52);
  self.button4.TabIndex := 5;
  self.button4.Text := 'Clear Text';
  self.button4.UseVisualStyleBackColor := true;
  self.button4.Click += new System.EventHandler(@self.button4_Click);
  // 
  // MainForm
  // 
  self.ClientSize := new System.Drawing.Size(1016, 661);
  self.Controls.Add(self.button4);
  self.Controls.Add(self.button3);
  self.Controls.Add(self.button2);
  self.Controls.Add(self.richTextBox1);
  self.Controls.Add(self.button1);
  self.Icon := (resources.GetObject('$this.Icon') as System.Drawing.Icon);
  self.Name := 'MainForm';
  self.Text := 'MainForm';
  self.Load += new System.EventHandler(@self.MainForm_Load);
  self.ResumeLayout(false);
end;
{$ENDREGION}

end.
