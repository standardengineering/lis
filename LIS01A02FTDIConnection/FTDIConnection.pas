﻿//Copyright (C) 2010 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.Connection;

interface

uses
  System.Threading,
  System.Text,
  Prism.StandardAspects,
  Essy.FTDIWrapper;

type
  Lis01A02FTDIConnection = public class(ILis01A2Connection)
  private
    fFTDIConnection: FTDI_Device;
    fAbortDataReceiveLoopWaitObject: EventWaitHandle := new EventWaitHandle(True, EventResetMode.ManualReset);
    fDataReceiveLoopReadyWaitObject: EventWaitHandle := new EventWaitHandle(True, EventResetMode.ManualReset);
    fAbortDataReceiveLoop: Boolean := false;
    method fDataReceiveLoop; async;
  protected
  public
    method WriteData(value: String);
    event OnReceiveString : LISConnectionReceivedDataEventHandler;
    method DisConnect;
    method Connect;
    method ClearBuffers;    
    [aspect:Disposable]
    method Dispose; 
    constructor(aDevice: FTDI_Device);
  end;
  
implementation

uses 
  System.Globalization, 
  System.Text;

method Lis01A02FTDIConnection.ClearBuffers;
begin
  fFTDIConnection.ClearReceiveBuffer();
  fFTDIConnection.ClearSendBuffer();
end;

method Lis01A02FTDIConnection.Connect;
begin
  var ok := false;
  var retryCounter := 0;
  repeat
    try
      fFTDIConnection.EnableWaitForDataEvent();
      fDataReceiveLoopReadyWaitObject.Reset();
      fDataReceiveLoop(); //start receive thread
      fDataReceiveLoopReadyWaitObject.WaitOne(); //wait for data receive loop to start
      ok := true;
    except  
      inc(retryCounter);
      if retryCounter > 4 then raise;
      fFTDIConnection.TryReloadDevice();
    end;   
  until ok;
end;

method Lis01A02FTDIConnection.DisConnect;
begin
  fAbortDataReceiveLoop := true; //stop receive thread
  fFTDIConnection.SignalDataWaitEvent();
  fAbortDataReceiveLoopWaitObject.WaitOne(2000);
  fFTDIConnection.DisableWaitForDataEvent();
end;

method Lis01A02FTDIConnection.WriteData(value: System.String);
begin
  var byteArray := Encoding.ASCII.GetBytes(value);
  fFTDIConnection.WriteBytes(byteArray);
end;

constructor Lis01A02FTDIConnection(aDevice: FTDI_Device);
begin
  self.fFTDIConnection := aDevice;
end;

method Lis01A02FTDIConnection.Dispose;
begin
  fAbortDataReceiveLoop := true;
  fAbortDataReceiveLoopWaitObject.WaitOne(2000);
  disposeAndNil(self.fFTDIConnection); //clean up internal FTDI device
end;

method Lis01A02FTDIConnection.fDataReceiveLoop;
begin
  fAbortDataReceiveLoopWaitObject.Reset();
  fAbortDataReceiveLoop := false;
  try
    fDataReceiveLoopReadyWaitObject.Set();
    loop
    begin     
      fFTDIConnection.WaitForData();
      if fAbortDataReceiveLoop then exit;
      var bytesReady := fFTDIConnection.NumberOfBytesInReceiveBuffer;
      if bytesReady > 0 then
      begin
        var data := fFTDIConnection.ReadBytes(bytesReady);
        var text := Encoding.ASCII.GetString(data);
        OnReceiveString:Invoke(self, new LISConnectionReceivedDataEventArgs(text));
      end;
    end;
  finally
    fAbortDataReceiveLoopWaitObject.Set();
  end;
end;



end.
