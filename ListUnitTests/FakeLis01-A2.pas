﻿namespace ListUnitTests;

interface

uses
  System.Collections.Generic,
  System.Linq,
  System.Text,
  Essy.LIS.Connection;

type
  FakeLis01A2 = public class(ILisConnection)
  private
  protected
  public
    event OnReceiveString : Essy.LIS.Connection.LISConnectionReceivedDataEventHandler raise;
    event OnReceiveTimeOut : System.EventHandler;
    event OnLISConnectionClosed : System.EventHandler;
    property Status: Essy.LIS.Connection.LisConnectionStatus;
    property MaxFrameSize: Integer read 64000;
    method SendIntermediateFrame(FrameNumber: System.Int32; aString: System.String);
    method Dispose;
    method StartReceiveTimeoutTimer;
    method StopSendMode;
    method EstablishSendMode: System.Boolean;
    method DisConnect;
    method Connect;
    method SendEndFrame(FrameNumber: System.Int32; aString: System.String);
    method FakeReceiveData(aData: String);
  end;

implementation

method FakeLis01A2.SendIntermediateFrame(FrameNumber: System.Int32; aString: System.String);
begin
end;

method FakeLis01A2.SendEndFrame(FrameNumber: System.Int32; aString: System.String);
begin
end;

method FakeLis01A2.Connect;
begin
end;

method FakeLis01A2.DisConnect;
begin
end;

method FakeLis01A2.EstablishSendMode: System.Boolean;
begin
  exit true;
end;

method FakeLis01A2.StopSendMode;
begin
end;

method FakeLis01A2.StartReceiveTimeoutTimer;
begin
end;

method FakeLis01A2.Dispose;
begin
end;

method FakeLis01A2.FakeReceiveData(aData: String);
begin
  OnReceiveString(self, new LISConnectionReceivedDataEventArgs(aData));
end;

end.
