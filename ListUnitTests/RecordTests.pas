﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace ListUnitTests;

interface

uses
  Essy.LIS.LIS02A2,
  NUnit.Framework,
  FluentAssertions,
  Moq;

type
  [TestFixture]
  [SetCulture("en-US")]
  RecordTests = public class
  private
  protected
  public
    [Test]
    method HeaderRecordTest;
    [Test]
    method HeaderRecordTest2;
    [Test]
    method QueryRecordTest;
    [Test]
    method PatientRecordTest;
    [Test]
    method PatientRecordTest2;
    [Test]
    method PatientRecordTest3;
    [Test]
    method OrderRecordTest;
    [Test]
    method OrderRecordTest2;
    [Test]
    method OrderRecordTest3;
    [Test]
    method ResultRecordTest1;
    [Test]
    method ResultRecordTest2;
    [Test]
    method ResultRecordTest3;
    [Test]
    method ResultRecordTest4;
    [Test]
    method ResultRecordTest5;
    [Test]
    method TerminatorRecordTest;
    [Test]
    method MultipleRecordsInFrameTest1;
  end;
  
implementation

uses 
  System.Runtime.InteropServices;

method RecordTests.HeaderRecordTest;
begin
  var lisString := 'H|\^&|||Thunder&F&bolt EIA^0.42.0.73|||||||P|LIS2-A2|20101022162157'; //also test of an escape code
  var h := new HeaderRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.QueryRecordTest;
begin
  var lisString := 'Q|1|S001||ALL';
  var h := new QueryRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.PatientRecordTest;
begin
  var lisString := 'P|1||S001';
  var h := new PatientRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.OrderRecordTest;
begin
  var lisString := 'O|1|S004||^^^CMVIgG|R';
  var h := new OrderRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.ResultRecordTest1;
begin
  var lisString := 'R|1|^^^CMVIgG|1.33|ng/ml||||F';
  var h := new ResultRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.ResultRecordTest2;
begin
  var lisString := 'R|1|^^^CMVIgG|1.33^high|ng/ml||||F';
  var h := new ResultRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.ResultRecordTest3;
begin
  var lisString := 'R|1|^^^CMVIgG^Subtest1|1.33|ng/ml||||F';
  var h := new ResultRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.TerminatorRecordTest;
begin
  var lisString := 'L|1|N';
  var h := new TerminatorRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.ResultRecordTest4;
begin
  var lisString := 'R|1|^^^CMVIgG^Subtest1|1.33|ng/ml|||ASR|F';
  var h := new ResultRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.ResultRecordTest5;
begin
  var lisString := 'R|1|^^^CMVIgG^Subtest1|1.33|ng/ml|||AR|F';
  var h := new ResultRecord(lisString);
  Assert.AreEqual(lisString + #13, h.ToLISString());
end;

method RecordTests.PatientRecordTest2;
begin
  var lisString := 'P|1||ASP235243||ASPYRA-TEST^GREEN^||19490619^62^Y^|M|||||10019';
  var lisString2 := 'P|1||ASP235243||ASPYRA-TEST^GREEN||19490619|M|||||10019';
  var con := new Mock<Essy.LIS.Connection.ILisConnection>;
  var parser := new LISParser(con.Object);
  var r: AbstractLisRecord;
  parser.OnReceivedRecord += (s, e) ->
  begin
    r := e.ReceivedRecord;   
  end;
  con.Raise(m -> m.OnReceiveString += nil, new Essy.LIS.Connection.LISConnectionReceivedDataEventArgs(lisString));
  Assert.AreEqual(lisString2 + #13, r.ToLISString()); 
end;

method RecordTests.PatientRecordTest3;
begin
  var lisString := 'P|1||AXIS-120890003||ASPYRA-TEST||19800620^31^Y^|M|||||123456788||||||||||||';
  var lisString2 := 'P|1||AXIS-120890003||ASPYRA-TEST||19800620|M|||||123456788';
  var con := new Mock<Essy.LIS.Connection.ILisConnection>;
  var parser := new LISParser(con.Object);
  var r: AbstractLisRecord;
  parser.OnReceivedRecord += (s, e) ->
  begin
    r := e.ReceivedRecord;   
  end;
  con.Raise(m -> m.OnReceiveString += nil, new Essy.LIS.Connection.LISConnectionReceivedDataEventArgs(lisString));
  Assert.AreEqual(lisString2 + #13, r.ToLISString()); 
end;

method RecordTests.HeaderRecordTest2;
begin
  var lisString := 'H|\^&|||LIS|||||||P||20120410145254' + #13;
  var lisString2 := 'H|\^&|||LIS|||||||P|LIS2-A2|20120410145254';
  var con := new Mock<Essy.LIS.Connection.ILisConnection>;
  var parser := new LISParser(con.Object);
  var r: AbstractLisRecord;
  parser.OnReceivedRecord += (s, e) ->
  begin
    r := e.ReceivedRecord;   
  end;
  con.Raise(m -> m.OnReceiveString += nil, new Essy.LIS.Connection.LISConnectionReceivedDataEventArgs(lisString));
  Assert.AreEqual(lisString2 + #13, r.ToLISString()); 
end;

method RecordTests.MultipleRecordsInFrameTest1;
begin
  var lisString := 'H|\^&|||IM|||||||P|LIS2-A2|20121219121831' + #13 + 'P|1||BioRad-1' + #13 + 'P|2||BioRad-1' + #13 + 'P|3||BioRad-1' + #13 + 'P|4||BioRad-1' + #13 + 'P|5||BioRad-1'
   + #13 + 'P|6||BioRad-3' + #13 + 'P|7||BioRad-3' + #13 + 'P|8||BioRad-3' + #13 + 'P|9||BioRad-3' + #13 + 'P|10||BioRad-3' + #13 + 'P|11||1234567' + #13 + 'L|1|N' + #13;
  var con := new Mock<Essy.LIS.Connection.ILisConnection>;
  var parser := new LISParser(con.Object);
  var r: AbstractLisRecord;
  var counter := 0;
  parser.OnReceivedRecord += (s, e) ->
  begin
    r := e.ReceivedRecord;  
    inc(counter); 
  end;
  con.Raise(m -> m.OnReceiveString += nil, new Essy.LIS.Connection.LISConnectionReceivedDataEventArgs(lisString));
  Assert.AreEqual(counter, 13); 
end;

method RecordTests.OrderRecordTest2;
begin
  var lisString := 'O|1|2231709||^^^ANA-12IgG^CENP-B^dsDNA^Sm^Ro/SS-A 52|R';
  var con := new Mock<Essy.LIS.Connection.ILisConnection>;
  var parser := new LISParser(con.Object);
  var r: AbstractLisRecord;
  parser.OnReceivedRecord += (s, e) ->
  begin
    r := e.ReceivedRecord;   
  end;
  con.Raise(m -> m.OnReceiveString += nil, new Essy.LIS.Connection.LISConnectionReceivedDataEventArgs(lisString)); 
  r.ToLISString().Should.Be(lisString + #13);
end;

method RecordTests.OrderRecordTest3;
begin
   var lisString := 'O|1|2231709||^^^ANA-12IgG^CENP-B^dsDNA^Sm^Ro/SS-A 52|R';
  var con := new Mock<Essy.LIS.Connection.ILisConnection>;
  var parser := new LISParser(con.Object);
  var r: AbstractLisRecord;
  parser.OnReceivedRecord += (s, e) ->
  begin
    r := e.ReceivedRecord;   
  end;
  con.Raise(m -> m.OnReceiveString += nil, new Essy.LIS.Connection.LISConnectionReceivedDataEventArgs(lisString)); 
  var optFields := (r as OrderRecord).TestID.OptionalFields;
  optFields.Should.HaveCount(4,"there are 4 subfields");
  optFields[0].Should.Be("CENP-B");
  optFields[1].Should.Be("dsDNA");
  optFields[2].Should.Be("Sm");
  optFields[3].Should.Be("Ro/SS-A 52");
end;

end.
