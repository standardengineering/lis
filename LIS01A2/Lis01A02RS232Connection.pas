﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.Connection;

interface

uses
  System.Collections.Generic,
  System.Linq,
  System.Text,
  System.IO.Ports;

type

  Lis01A02RS232Connection = public class(ILis01A2Connection)
  private
    fComport: SerialPort;
    method set_ComPort(value: SerialPort);
    method COMPortDataReceived(Sender: Object; e: SerialDataReceivedEventArgs);
  protected
  public
    method WriteData(value: System.String);
    event OnReceiveString : Essy.LIS.Connection.LISConnectionReceivedDataEventHandler;
    method DisConnect;
    method Connect;
    method ClearBuffers;
    constructor(aComPort: SerialPort);
    Property ComPort: SerialPort read fComport write set_ComPort;
  end;

implementation

method Lis01A02RS232Connection.ClearBuffers;
begin
  self.fComport.DiscardInBuffer();
  self.fComport.DiscardOutBuffer();
end;

method Lis01A02RS232Connection.Connect;
begin
  self.fComport.Open();
end;

method Lis01A02RS232Connection.DisConnect;
begin
  self.fComport.Close();
end;

method Lis01A02RS232Connection.WriteData(value: System.String);
begin
  self.fComport.Write(value);
end;

constructor Lis01A02RS232Connection(aComPort: SerialPort);
begin
  inherited constructor;
  self.ComPort := aComPort;
end;

method Lis01A02RS232Connection.COMPortDataReceived(Sender: Object; e: SerialDataReceivedEventArgs);
begin
  var receivedData := self.ComPort.ReadExisting;
  self.OnReceiveString:Invoke(self, new LISConnectionReceivedDataEventArgs(receivedData));
end;

method Lis01A02RS232Connection.set_ComPort(value: SerialPort);
begin
  if assigned(self.fComport) then
  begin
    if self.fComport <> value then 
    begin
      self.fComport.DataReceived -= COMPortDataReceived;
    end;
  end;
  self.fComport := value;
  self.fComport.DataReceived += COMPortDataReceived;
  self.fComport.ReadTimeout := 15000;
end;

end.
