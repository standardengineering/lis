﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.Connection;

interface

uses
  System.IO.Ports, 
  System.Text,
  System.Threading,
  slf4net,
  Essy.LIS.LIS02A2,
  Prism.StandardAspects;

type

  /// <summary>
  /// This is the LIS01-A2 connection class, it encodes data according to CLSI LIS01-A2 Chapter 6
  /// </summary>
  Lis01A2Connection = public class(ILisConnection)
  private
    const STX: Char = #2;
    const ETX: Char = #3;
    const ETB: Char = #23;
    const ENQ: Char = #5;
    const ACK: Char = #6;
    const NAK: Char = #21;
    const EOT: Char = #4;
    const CR: Char = #13;
    const LF: Char = #10;
    const MAXFRAMESIZE = 63993; //LIS01-A2 8.3.1.2 or 6.3.1.2 (64000 chars - 7 chars overhead)
    var fLog: ILogger;
    var fConnection: ILis01A2Connection;
    var fTempReceiveBuffer: StringBuilder := new StringBuilder;
    var fTempIntermediateFrameBuffer: String;
    var fENQWaitObject: EventWaitHandle := new EventWaitHandle(true, EventResetMode.ManualReset);
    var fACKWaitObject: EventWaitHandle := new EventWaitHandle(true, EventResetMode.ManualReset);
    var fEOTWaitObject: EventWaitHandle := new EventWaitHandle(true, EventResetMode.ManualReset);
    var fAckReceived: Boolean;
    var fReceiveTimeOutTimer: System.Timers.Timer := new System.Timers.Timer;
    var fFrameNumber: Integer;
    var fLastFrameWasIntermediate: Boolean;
    method fReceiveTimeOutTimer_Elapsed(sender: Object; e: System.Timers.ElapsedEventArgs);
    method set_Connection(value: ILis01A2Connection);
    method WaitForACK: Boolean;
    method SendString(aString: String);
    method CalculateChecksum(aString: String): String;
    method CheckChecksum(aLine: String): Boolean;
    method ConnectionDataReceived(Sender: Object; e: LISConnectionReceivedDataEventArgs);
    method SendIntermediateFrame(frameNumber: Int32; aString: String); 
    method SendEndFrame(frameNumber: Int32; aString: String); 
  protected
  public
    /// <summary>
    /// Transmits the EOT transmission control character and then regards the data link to be in a neutral state.
    /// </summary>
    method StopSendMode;
    /// <summary>
    /// Transmits a message to the receiver.
    /// </summary>
    /// <param name="aMessage">The message to be transmitted, messages are sent in frames; each frame contains a maximum of 64 000 characters (including frame
    /// overhead). Messages longer than 64 000 characters are divided between two or more frames.</param>   
    method SendMessage(aMessage: String);
    /// <summary>
    /// Initiates the Establishment Phase, see CLSI LIS01-A2 Chapter 6.2
    /// </summary>
    /// <returns>True when the receiving system answered with an ACK</returns>
    method EstablishSendMode: Boolean; 
    /// <summary>
    /// Disconnects the low level connection, e.g. closes the COM-port or closes the TCP/IP Socket
    /// </summary>
    method DisConnect; 
    /// <summary>
    /// Connectes the low level connection, e.g. opens the COM-port or opens the TCP/IP Socket
    /// </summary>
    method Connect; 
    /// <summary>
    /// Starts the Receive Timeout counter, see CLSI LIS01-A2 Chapter 6.5.2
    /// </summary>
    method StartReceiveTimeoutTimer;
    [aspect:Disposable]
    /// <summary>
    /// Disposes this object.
    /// </summary>
    method Dispose;
    /// <summary>
    /// This event is triggered when incoming message is received.
    /// </summary>
    event OnReceiveString: LISConnectionReceivedDataEventHandler;
    /// <summary>
    /// This event is fired when the low level connection is successfully closed.
    /// </summary>
    event OnLISConnectionClosed: EventHandler;
    /// <summary>
    /// This event fires when a receive timeout is detected, see CLSI LIS01-A2 Chapter 6.5.2
    /// </summary>
    event OnReceiveTimeOut: EventHandler;
    /// <summary>
    /// The state of the connection
    /// </summary>
    /// <value>Idle, Sending, Receiving, Establishing</value>
    property Status: LisConnectionStatus;
    /// <summary>
    /// This is the low level connection object that is used to put the frames on the wire, e.g. a COM-Port or TCP Socket connection.
    /// </summary>
    /// <value>An implementation of the ILis01A2Connection Interface.</value>
    Property Connection: ILis01A2Connection read fConnection write set_Connection;
    /// <summary>
    ///  Creates a new Lis01A2 Connection object.
    /// </summary>
    /// <param name="aConnection">An implementation of the ILis01A2Connection Interface.</param>
    constructor(aConnection: ILis01A2Connection);
    /// <summary>
    /// Creates a new Lis01A2 Connection object.
    /// </summary>
    /// <param name="aConnection">An implementation of the ILis01A2Connection Interface.</param>
    /// <param name="aTimeOut">A receive timeout in seconds.</param>
    constructor(aConnection: ILis01A2Connection; aTimeOut: Integer);
  end;

  /// <summary>
  /// A custom exception that is uses for all LIS01-A2 related exceptions.
  /// </summary>
  Lis01A2ConnectionException = public class(LisConnectionException);
  
implementation

uses 
  System.Runtime.CompilerServices,
  System.Threading;

constructor Lis01A2Connection(aConnection: ILis01A2Connection; aTimeOut: Integer);
begin
  inherited constructor;
  self.Connection := aConnection;
  self.fReceiveTimeOutTimer.Interval := aTimeOut * 1000; //Seconds to miliseconds 
  self.fReceiveTimeOutTimer.Enabled := false;
  self.fReceiveTimeOutTimer.Elapsed += fReceiveTimeOutTimer_Elapsed;
  fLog := slf4net.LoggerFactory.GetLogger(typeOf(Lis01A2Connection));
end;

constructor Lis01A2Connection(aConnection: ILis01A2Connection);
begin
  constructor(aConnection, 30); //30 seconds receive timeout (see 6.5.2.4 in LIS01-A2)
end;

method Lis01A2Connection.Connect;
begin
  try
    fLog.Info(Environment.NewLine + "Connecting To LIS...");
    Connection.Connect();
    fLog.Info("Connected");
  except
    on ex: Exception do 
    begin
      fLog.Error("Error Opening Connection" + Environment.NewLine + ex.Message);
      raise new Lis01A2ConnectionException('Error opening Connection.', ex);
    end;
  end;
end;

method Lis01A2Connection.DisConnect;
begin
  if (self.Status <> LisConnectionStatus.Idle) and (not self.fEOTWaitObject.WaitOne(15000)) then 
  begin
    self.Status := LisConnectionStatus.Idle;
    raise new Lis01A2ConnectionException('Error closing Connection, no EOT received.');
  end;

  try
    fLog.Info("Disconnecting...");
    Connection.DisConnect();
    self.Status := LisConnectionStatus.Idle;
    fLog.Info("Disconnected");
    if assigned(self.OnLISConnectionClosed) then
    begin
      self.OnLISConnectionClosed.Invoke(self, new EventArgs);
    end;
  except
    on ex: Exception do 
    begin
      fLog.Error("Error Closing Connection" + Environment.NewLine + ex.Message);
      raise new Lis01A2ConnectionException('Error closing Connection.', ex);
    end;
  end;
end;

method Lis01A2Connection.EstablishSendMode: Boolean;
begin
  result := true;
  fFrameNumber := 1; //reset framecounter
  if self.Status <> LisConnectionStatus.Idle then
  begin
    fLog.Error("Connection not idle when trying to establish send mode");
    raise new Lis01A2ConnectionException('Connection not idle when trying to establish send mode');
  end;
  self.Status := LisConnectionStatus.Establishing;
  fLog.Info("Establishing send mode");
  self.Connection.ClearBuffers();
  self.fENQWaitObject.Reset();
  self.Connection.WriteData(ENQ); //Send ENQ to LIS to request Send Mode.
  fLog.Debug("send <ENQ>");
  self.fENQWaitObject.WaitOne(15000, false); //timeout of 15 seconds (see 6.5.2.1 in LIS01-A2)
  if (self.Status <> LisConnectionStatus.Sending) then
  begin
    StopSendMode();
    exit(false);
  end;
end;

method Lis01A2Connection.SendEndFrame(frameNumber: Int32; aString: String);
begin
  fLog.Info(frameNumber.ToString + aString);
  fLog.Debug("send <ETX>");
  SendString(frameNumber.ToString + aString + ETX);
end;

method Lis01A2Connection.SendIntermediateFrame(frameNumber: Int32; aString: String);
begin
  fLog.Info(frameNumber.ToString + aString);
  fLog.Debug("send <ETB>");
  SendString(frameNumber.ToString + aString + ETB);
end;

method Lis01A2Connection.StopSendMode;
begin
  self.Connection.WriteData(EOT); //Send EOT to return to idle mode
  fLog.Debug("send <EOT>");
  self.Status := LisConnectionStatus.Idle;
end;

method Lis01A2Connection.WaitForACK: Boolean;
begin
  fACKWaitObject.Reset();
  if not fACKWaitObject.WaitOne(15000) then //timeout of 15 seconds (see 6.5.2.3 in LIS01-A2)
  begin
    StopSendMode();
    fLog.Error('No response from LIS within timeout period.');
    raise new Lis01A2ConnectionException('No response from LIS within timeout period.');
  end;
  if fAckReceived then 
	begin
  	fLog.Debug("Received <ACK>");
	end
	else 
	begin
  	fLog.Debug("<ACK> expected, but other character received.");
	end;
  result := fAckReceived;
end;

method Lis01A2Connection.SendString(aString: String);
begin
  if self.Status = LisConnectionStatus.Sending then
  begin
    self.Connection.ClearBuffers();
    var tryCounter := 0;
    var tempSendString := STX + aString + CalculateChecksum(aString) + CR + LF;

    self.Connection.WriteData(tempSendString);
    while not WaitForACK do
    begin
      inc(tryCounter);
      if tryCounter > 5 then //try 6 times (see 6.5.1.2 in LIS01-A2)
      begin
        StopSendMode();
        fLog.Error('Max number of send retries reached.');
        raise new Lis01A2ConnectionException('Max number of send retries reached.');
      end;
      self.Connection.WriteData(tempSendString);  
    end;
  end
  else
  begin
    fLog.Error('Connection not in Send mode when trying to send data.');
    raise new Lis01A2ConnectionException('Connection not in Send mode when trying to send data.');
  end;
end;

method Lis01A2Connection.CalculateChecksum(aString: String): String;
begin
  var total: Int32 := 0;
  for each i in aString do
  begin
    total := total + ord(i);
  end;
  var tempVal: Byte := total mod 256;
  result := tempVal.ToString('X2');
end;

method Lis01A2Connection.ConnectionDataReceived(Sender: Object; e: LISConnectionReceivedDataEventArgs);
begin
  var buffer := e.ReceivedData;
  for each ch in buffer do
  begin
    case self.Status of
      LisConnectionStatus.Idle:
        begin
          if ch = ENQ then // If incoming Char is ENQ
          begin
            fLog.Debug("received <ENQ>");
            self.Connection.WriteData(ACK); // Send ACK
            fLog.Debug("send <ACK>");
            self.Status := LisConnectionStatus.Receiving; // set status to receiving
            fEOTWaitObject.Reset();
            self.fTempReceiveBuffer.Clear();
            fTempIntermediateFrameBuffer := String.Empty;
            fReceiveTimeOutTimer.Enabled := true;
          end
          else
          begin
            fLog.Debug("send <NAK>");
            self.Connection.WriteData(NAK); // Send NAK
          end;
        end;
      LisConnectionStatus.Establishing:
        begin 
          if ch = ACK then // If incoming Char is ACK
          begin
            fLog.Debug("received <ACK>");
            self.Status := LisConnectionStatus.Sending;
            self.fENQWaitObject.Set();
            exit;
          end; 
          if ch = ENQ then // If incoming Char is ENQ = Contention (see 6.2.7.1 in LIS01-A2)
          begin
            fLog.Debug("received <ENQ>");
            Thread.Sleep(1000);
            fLog.Debug("send <ENQ>");
            self.Connection.WriteData(ENQ); // Send ENQ
            exit;
          end;
        end;
      LisConnectionStatus.Sending:
        begin 
          fAckReceived := (ch = ACK);
          fACKWaitObject.Set();       
        end;
      LisConnectionStatus.Receiving:
        begin
          self.fReceiveTimeOutTimer.Stop();
          self.fReceiveTimeOutTimer.Start(); //reset timer
          if ch = ENQ then // If incoming Char is ENQ
          begin
            fLog.Debug("received <ENQ>");
            self.Connection.WriteData(NAK); // Send NAK
            fLog.Debug("send <NAK>");
            exit;
          end;
          if ch = EOT then // If incoming Char is EOT
          begin
            self.fReceiveTimeOutTimer.Stop();
            fLog.Debug("received <EOT>");
            self.Status := LisConnectionStatus.Idle; // set status to Idle
            self.fEOTWaitObject.Set;         
            exit;
          end;
          if ch <> #0 then self.fTempReceiveBuffer.Append(ch);
          if ch = LF then
          begin
            var tempReceiveBuffer := self.fTempReceiveBuffer.ToString;
            if CheckChecksum(tempReceiveBuffer) then
            begin
              fLog.Debug("send <ACK>");
              self.Connection.WriteData(ACK); // Send ACK
              var cleanReceiveBuffer := tempReceiveBuffer.Substring(2, tempReceiveBuffer.Length - 7); //copy everything except the STX, framenumber, checksum, CR and LF   
              if fLastFrameWasIntermediate then
              begin
                fTempIntermediateFrameBuffer := fTempIntermediateFrameBuffer + cleanReceiveBuffer;
              end
              else 
              begin
                if assigned(self.OnReceiveString) then
                begin
                  var line := fTempIntermediateFrameBuffer + cleanReceiveBuffer;
                  var arg := new LISConnectionReceivedDataEventArgs(line);
                  self.OnReceiveString(self, arg); 
                  fLog.Info("received: " + line);
                end;
              end;             
              self.fTempReceiveBuffer.Clear;
              fTempIntermediateFrameBuffer := String.Empty;
            end
            else
            begin
              fLog.Debug("send <NAK>");
              self.Connection.WriteData(NAK); // Send NAK
              self.fTempReceiveBuffer.Clear;
            end;
          end;         
        end;
    end;
  end;
end;

method Lis01A2Connection.set_Connection(value: ILis01A2Connection);
begin
  if assigned(self.fConnection) then
  begin
    if self.fConnection <> value then 
    begin
      self.fConnection.OnReceiveString -= ConnectionDataReceived;
    end;
  end;
  self.fConnection := value;
  self.fConnection.OnReceiveString += ConnectionDataReceived;
end;

method Lis01A2Connection.CheckChecksum(aLine: String): Boolean;
begin
  result := false;
  var lineLength := aLine.Length;
  if lineLength < 5 then exit;
  if aLine[0] <> STX then exit;
  if aLine[lineLength - 1] <> LF then exit;
  if aLine[lineLength - 2] <> CR then exit;
  var etxOrEtb := aLine[lineLength - 5];
  if (etxOrEtb <> ETX) and (etxOrEtb <> ETB) then exit;
  fLastFrameWasIntermediate := (etxOrEtb = ETB);
  if aLine[lineLength - 6] <> CR then exit;
  var tempChecksum := aLine.Substring(lineLength - 4, 2); //copy the checksum from the received String.
  var tempLine := aLine.Substring(1, lineLength - 5); //copy the original high level record + ETX.
  var tempCheckSum2 := CalculateChecksum(tempLine);
  if tempChecksum <> tempCheckSum2 then exit; // if checksum is not the same then exit.
  result := true;
end;

method Lis01A2Connection.fReceiveTimeOutTimer_Elapsed(sender: Object; e: System.Timers.ElapsedEventArgs);
begin
  self.Status := LisConnectionStatus.Idle; // set status to Idle
  self.fReceiveTimeOutTimer.Stop();
  if assigned(self.OnReceiveTimeOut) then
  begin
    self.OnReceiveTimeOut(self, new EventArgs);
  end;
end;

method Lis01A2Connection.StartReceiveTimeoutTimer;
begin
  self.fReceiveTimeOutTimer.Start();
end;

method Lis01A2Connection.Dispose;
begin
  var dis := IDisposable(Connection);  
  dis:Dispose();
end;

method Lis01A2Connection.SendMessage(aMessage: String);
begin
  while aMessage.Length > MAXFRAMESIZE do //LIS01-A2 6.3.1.2 or 8.3.1.2
  begin  
    var intermediateFrame := aMessage.Substring(0, MAXFRAMESIZE);
    SendIntermediateFrame(fFrameNumber, intermediateFrame);
    aMessage := aMessage.Remove(0, MAXFRAMESIZE); 
    inc(fFrameNumber);
    if fFrameNumber > 7 then fFrameNumber := 0;
  end;
  SendEndFrame(fFrameNumber, aMessage);
  inc(fFrameNumber);
  if fFrameNumber > 7 then fFrameNumber := 0;
end;



end.