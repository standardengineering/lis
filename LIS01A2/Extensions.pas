﻿namespace Essy.Lis.Connection;

interface

uses
  System.Text,
  System.Runtime.CompilerServices;

type
  [Extension]
  GPExtensions = public static class
  public
    [Extension]
    class method Clear(aStringBuilder: StringBuilder);
  end;
  
implementation

class method GPExtensions.Clear(aStringBuilder: StringBuilder);
begin
  aStringBuilder.Length := 0;
end;

end.