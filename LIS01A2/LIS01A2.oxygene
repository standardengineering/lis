<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <RootNamespace>Essy.LIS.Connection</RootNamespace>
    <OutputType>Library</OutputType>
    <AssemblyName>LIS01A2</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <Configuration Condition="'$(Configuration)' == ''">Release</Configuration>
    <TargetFrameworkVersion>v4.6</TargetFrameworkVersion>
    <ProjectGuid>{8AE6DAF6-B7CA-41DC-B706-0C7072B922E4}</ProjectGuid>
    <Company />
    <InternalAssemblyName />
    <StartupClass />
    <DefaultUses />
    <ApplicationIcon />
    <TargetFrameworkProfile />
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
    <ProjectView>ShowAllFiles</ProjectView>
    <XmlDoc>True</XmlDoc>
    <GenerateDebugInfo>True</GenerateDebugInfo>
    <GenerateMDB>True</GenerateMDB>
    <NuGetPackageImportStamp>
    </NuGetPackageImportStamp>
    <RepositoryUrl>https://bitbucket.org/standardengineering/lis.git</RepositoryUrl>
    <RepositoryType>git</RepositoryType>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <OutputPath>bin\Debug\</OutputPath>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>True</GenerateMDB>
    <CpuType>anycpu</CpuType>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <CheckForOverflowUnderflow>True</CheckForOverflowUnderflow>
    <WarnOnCaseMismatch>True</WarnOnCaseMismatch>
    <RunCodeAnalysis>True</RunCodeAnalysis>
    <RequireExplicitLocalInitialization>True</RequireExplicitLocalInitialization>
    <EnableUnmanagedDebugging>False</EnableUnmanagedDebugging>
    <DebugClass />
    <AssertMethodName />
    <UseXmlDoc>True</UseXmlDoc>
    <CodeAnalysis>True</CodeAnalysis>
    <CodeAnalysisSet>default</CodeAnalysisSet>
    <CodeAnalysisSeverity>High</CodeAnalysisSeverity>
    <CodeAnalysisConfidence>Normal</CodeAnalysisConfidence>
    <CodeAnalysisIgnoreFile>Properties\CodeAnalysis.ignore</CodeAnalysisIgnoreFile>
    <Optimize>False</Optimize>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Release' ">
    <OutputPath>..\lib\net46</OutputPath>
    <EnableAsserts>False</EnableAsserts>
    <GenerateDebugInfo>True</GenerateDebugInfo>
    <GenerateMDB>True</GenerateMDB>
    <GeneratePDB>True</GeneratePDB>
    <XmlDoc>True</XmlDoc>
  </PropertyGroup>
  <ItemGroup>
    <ProjectReference Include="..\LIS02A2\LIS02A2.oxygene">
      <Name>LIS02A2</Name>
      <Project>{42a49954-6fe1-4200-96e5-924509051b8e}</Project>
      <Private>True</Private>
      <HintPath>..\LIS02A2\bin\Debug\E1394-91.dll</HintPath>
    </ProjectReference>
    <ProjectReference Include="..\oxygeneaspects\trunk\Source\Prism.StandardAspects\Prism.StandardAspects.oxygene">
      <Name>Prism.StandardAspects</Name>
      <Project>{fdfa9e51-2441-4785-9599-44a17d1b72c7}</Project>
      <Private>True</Private>
      <HintPath>..\oxygeneaspects\trunk\Source\Prism.StandardAspects\bin\Debug\Prism.StandardAspects.dll</HintPath>
    </ProjectReference>
    <Reference Include="mscorlib">
      <HintPath>mscorlib.dll</HintPath>
    </Reference>
    <Reference Include="slf4net">
      <HintPath>..\packages\slf4net.0.1.32.1\lib\net35\slf4net.dll</HintPath>
      <Private>True</Private>
    </Reference>
    <Reference Include="System">
      <HintPath>System.dll</HintPath>
    </Reference>
  </ItemGroup>
  <ItemGroup>
    <Compile Include="Extensions.pas" />
    <Compile Include="Lis01A02RS232Connection.pas" />
    <Compile Include="Lis01A02TCPConnection.pas" />
    <Compile Include="Lis01A2Connection.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <Compile Include="Lis01A2.pas" />
    <Compile Include="Lis01A02Enums.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <ItemGroup>
    <Content Include="..\ESSY.LIS.LIS01.nuspec">
      <SubType>Content</SubType>
      <Link>ESSY.LIS.LIS01.nuspec</Link>
    </Content>
    <Content Include="app.config">
      <SubType>Content</SubType>
    </Content>
    <Content Include="packages.config">
      <SubType>Content</SubType>
    </Content>
    <Content Include="Properties\CodeAnalysis.ignore">
      <SubType>Content</SubType>
    </Content>
  </ItemGroup>
  <PropertyGroup>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'NugetBuild' ">
    <EnableAsserts>False</EnableAsserts>
    <GenerateDebugInfo>True</GenerateDebugInfo>
    <GenerateMDB>True</GenerateMDB>
    <GeneratePDB>True</GeneratePDB>
    <XmlDoc>True</XmlDoc>
    <OutputPath>..\lib\net46</OutputPath>
    <CheckForOverflowUnderflow>True</CheckForOverflowUnderflow>
  </PropertyGroup>
  <PropertyGroup>
  </PropertyGroup>
  <Target Name="EnsureNuGetPackageBuildImports" BeforeTargets="PrepareForBuild">
    <PropertyGroup>
      <ErrorText>This project references NuGet package(s) that are missing on this computer. Use NuGet Package Restore to download them.  For more information, see http://go.microsoft.com/fwlink/?LinkID=322105. The missing file is {0}.</ErrorText>
    </PropertyGroup>
    <Error Condition="!Exists('..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.props')" Text="$([System.String]::Format('$(ErrorText)', '..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.props'))" />
    <Error Condition="!Exists('..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.targets')" Text="$([System.String]::Format('$(ErrorText)', '..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.targets'))" />
    <Error Condition="!Exists('..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.props')" Text="$([System.String]::Format('$(ErrorText)', '..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.props'))" />
    <Error Condition="!Exists('..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.targets')" Text="$([System.String]::Format('$(ErrorText)', '..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.targets'))" />
    <Error Condition="!Exists('..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.props')" Text="$([System.String]::Format('$(ErrorText)', '..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.props'))" />
    <Error Condition="!Exists('..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.targets')" Text="$([System.String]::Format('$(ErrorText)', '..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.targets'))" />
  </Target>
  <Import Project="..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.props" Condition="Exists('..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.props')" />
  <Import Project="..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.props" Condition="Exists('..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.props')" />
  <Import Project="..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.props" Condition="Exists('..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.props')" />
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.targets" />
  <Import Project="..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.targets" Condition="Exists('..\packages\Microsoft.Build.Tasks.Git.1.0.0\build\Microsoft.Build.Tasks.Git.targets')" />
  <Import Project="..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.targets" Condition="Exists('..\packages\Microsoft.SourceLink.Common.1.0.0\build\Microsoft.SourceLink.Common.targets')" />
  <Import Project="..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.targets" Condition="Exists('..\packages\Microsoft.SourceLink.Bitbucket.Git.1.0.0\build\Microsoft.SourceLink.Bitbucket.Git.targets')" />
  <PropertyGroup>
    <PreBuildEvent>
    </PreBuildEvent>
    <PostBuildEvent />
  </PropertyGroup>
</Project>