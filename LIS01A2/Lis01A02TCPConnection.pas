﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.Connection;

interface

uses
  System.Collections.Generic,
  System.Threading,
  System.Net,
  System.Net.Sockets,
  System.Text,
  slf4net;

type

  Lis01A02TCPConnection = public class(ILis01A2Connection)
  private
    var fSocket: Socket ;
    var fLog: ILogger;
    fIsInServerMode : Boolean := false;
    fClient: TcpClient;
    method ReceiveLoop; async;
  protected
  public
    property NetWorkAddress: String;
    property NetWorkPort: UInt16;
    method WriteData(value: System.String);
    event OnReceiveString : LISConnectionReceivedDataEventHandler;
    method DisConnect;
    method Connect;
    method ClearBuffers;
    method StartListeningAsync; async;
    constructor(aNetWorkAddress: String; aNetWorkPort: UInt16);
  end;

  Lis01A02TCPConnectionException = public class(LisConnectionException);

implementation

method Lis01A02TCPConnection.ClearBuffers;
begin
end;

method Lis01A02TCPConnection.Connect;
begin
  if fIsInServerMode then exit;
  fSocket := new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);  //create a standard IPv4 TCP socket
  if not fSocket.Connected then
  begin
    fSocket.Connect(self.NetWorkAddress, self.NetWorkPort);
    if fSocket.Connected then
    begin
      ReceiveLoop();
    end;
  end
  else
  begin
    raise new Lis01A02TCPConnectionException('Could not connect to server because the connection is already open');
  end;
end;

method Lis01A02TCPConnection.DisConnect;
begin
  if fSocket.Connected then fSocket.Close;
  disposeAndNil(fSocket);
end;

method Lis01A02TCPConnection.WriteData(value: System.String);
begin
  var buffer := Encoding.UTF8.GetBytes(value);
  fSocket.Send(buffer);
end;

constructor Lis01A02TCPConnection(aNetWorkAddress: String; aNetWorkPort: UInt16);
begin
  inherited constructor;
  self.NetWorkAddress := aNetWorkAddress;
  self.NetWorkPort := aNetWorkPort;
  fLog := slf4net.LoggerFactory.GetLogger(typeOf(Lis01A02TCPConnection));
end;

method Lis01A02TCPConnection.ReceiveLoop;
begin
  try
    var buffer := new Byte[1024];  //1KByte buffer
    while (assigned(fSocket) and fSocket.Connected) do
    begin
      var ab := fSocket.Available;
      if ab > 0 then
      begin
        var ar := fSocket.Receive(buffer);
        var receivedString := Encoding.UTF8.GetString(buffer, 0, ar);
        self.OnReceiveString:Invoke(self, new LISConnectionReceivedDataEventArgs(receivedString));
      end
      else
      begin
        Thread.Sleep(5);  //sleep 5 mS
      end;
    end;
  except
    on ex: SocketException where ex.ErrorCode = 10057 do
    begin
      fLog.Error(ex.Message); // Log the exception and exit to stop receiving.
    end;
  end;
end;

method Lis01A02TCPConnection.StartListeningAsync ;
begin
  fIsInServerMode := true;

  var localAddr := IPAddress.Parse(NetWorkAddress);
  var server := new TcpListener(localAddr,NetWorkPort);
  // Start listening for client Requests
  server.Start();
  // Buffer for reading data
  var bytes := new Byte[1024];
  var data : String := nil;

  // Start listening loop
  loop 
  begin
    fClient:= server.AcceptTcpClient();
    fSocket := fClient.Client;
    data := nil;
    
    // Get a stream object for reading and writing
    var stream := fClient.GetStream();
    var i := stream.Read(bytes, 0 , bytes.Length);
    while not (i=0) do
    begin
      data := System.Text.Encoding.ASCII.GetString(bytes, 0, i);
      OnReceiveString:Invoke(self, new LISConnectionReceivedDataEventArgs(data));
      i := stream.Read(bytes, 0 , bytes.Length);
    end;

    fClient.Close();
  end;
 
end;

end.
