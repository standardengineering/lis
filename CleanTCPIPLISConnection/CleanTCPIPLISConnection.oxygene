<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <RootNamespace>Essy.LIS.Connection</RootNamespace>
    <OutputType>Library</OutputType>
    <AssemblyName>TCPIPConnection</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <Configuration Condition="'$(Configuration)' == ''">Release</Configuration>
    <TargetFrameworkVersion>v4.6</TargetFrameworkVersion>
    <ProjectGuid>{AA615406-384D-49FD-9D2E-FE83E0FA1105}</ProjectGuid>
    <Company />
    <InternalAssemblyName />
    <StartupClass />
    <DefaultUses />
    <ApplicationIcon />
    <TargetFrameworkProfile />
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
    <ProjectView>ShowAllFiles</ProjectView>
    <OutputPath>..\lib\net46</OutputPath>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <OutputPath>..\lib\net46</OutputPath>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>True</GenerateMDB>
    <SuppressWarnings />
    <CpuType>anycpu</CpuType>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <WarnOnCaseMismatch>True</WarnOnCaseMismatch>
    <RunCodeAnalysis>True</RunCodeAnalysis>
    <FutureHelperClassName />
    <CheckForOverflowUnderflow>True</CheckForOverflowUnderflow>
    <RequireExplicitLocalInitialization>True</RequireExplicitLocalInitialization>
    <Optimize>False</Optimize>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Release' ">
    <OutputPath>..\lib\net46</OutputPath>
    <EnableAsserts>False</EnableAsserts>
    <GenerateDebugInfo>True</GenerateDebugInfo>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="mscorlib">
      <HintPath>mscorlib.dll</HintPath>
    </Reference>
    <Reference Include="slf4net">
      <HintPath>..\packages\slf4net.0.1.32.1\lib\net35\slf4net.dll</HintPath>
      <Private>True</Private>
    </Reference>
    <Reference Include="System">
      <HintPath>System.dll</HintPath>
    </Reference>
    <Reference Include="System.Data">
      <HintPath>System.Data.dll</HintPath>
    </Reference>
    <Reference Include="System.Xml">
      <HintPath>System.Xml.dll</HintPath>
    </Reference>
  </ItemGroup>
  <ItemGroup>
    <Compile Include="Extensions.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
    <Compile Include="TCPIP.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <ItemGroup>
    <ProjectReference Include="..\LIS01A2\LIS01A2.oxygene">
      <Name>LIS01A2</Name>
      <Project>{8ae6daf6-b7ca-41dc-b706-0c7072b922e4}</Project>
      <Private>True</Private>
      <HintPath>..\LIS01A2\bin\Debug\LIS01A2.dll</HintPath>
    </ProjectReference>
    <ProjectReference Include="..\LIS02A2\LIS02A2.oxygene">
      <Name>LIS02A2</Name>
      <Project>{42a49954-6fe1-4200-96e5-924509051b8e}</Project>
      <Private>True</Private>
      <HintPath>..\LIS02A2\bin\Debug\E1394-91.dll</HintPath>
    </ProjectReference>
  </ItemGroup>
  <ItemGroup>
    <Content Include="..\Essy.LIS.Connection.TCPIPLisConnection.nuspec">
      <SubType>Content</SubType>
      <Link>Essy.LIS.Connection.TCPIPLisConnection.nuspec</Link>
    </Content>
    <Content Include="app.config">
      <SubType>Content</SubType>
    </Content>
    <Content Include="packages.config">
      <SubType>Content</SubType>
    </Content>
  </ItemGroup>
  <PropertyGroup>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'NugetBuild' ">
    <EnableAsserts>False</EnableAsserts>
    <GenerateDebugInfo>True</GenerateDebugInfo>
    <OutputPath>bin\NugetBuild\</OutputPath>
  </PropertyGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.targets" />
  <PropertyGroup>
    <PreBuildEvent>
    </PreBuildEvent>
    <PostBuildEvent />
  </PropertyGroup>
</Project>