﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.Connection;

interface

uses
  System,
  System.Net,
  System.Net.Sockets,
  System.Text,
  Prism.StandardAspects,
  slf4net,
  Essy.LIS.LIS02A2,
  Essy.GeneralPurposeExtensionMethods;

type

  TCPIPLisConnection = public class(ILisConnection)
  private
    const CR: Char = #13;
    var fSocket: Socket ; //create a standard IPv4 TCP socket
    var fTempReceiveBuffer: StringBuilder := new StringBuilder;
    var fReceiveTimeOutTimer: System.Timers.Timer := new System.Timers.Timer;
    fIsInServerMode : Boolean := false;
    fClient: TcpClient;
    var fLog: ILogger;
    method fReceiveTimeOutTimer_Elapsed(sender: Object; e: System.Timers.ElapsedEventArgs);
    method ReceiveLoop; async;
    method SendString(aString: String);
  protected
  public
    method SendMessage(aMessage: String);
    method Connect;
    method DisConnect; 
    method EstablishSendMode: Boolean; 
    method StopSendMode;
    method StartReceiveTimeoutTimer;
    method StartListeningAsync; async;
    method StopListening(aTCPListener: TcpListener);
    [aspect:Disposable]
    method Dispose; 
    property NetWorkAddress: String;
    property NetWorkPort: UInt16;
    property Status: LisConnectionStatus;
    event OnReceiveString: LISConnectionReceivedDataEventHandler;
    event OnLISConnectionClosed: EventHandler;
    event OnReceiveTimeOut: EventHandler;
    constructor(aNetWorkAddress: String; aNetWorkPort: UInt16);
    /// <summary>
    /// overloaded constructor that allows you to overrule the receive timeout
    /// </summary>
    /// <param name="aNetWorkAddress">The server URL</param>
    /// <param name="aNetWorkPort">the TCP port</param>
    /// <param name="aTimeOut">The receive timeout in seconds</param>
    constructor(aNetWorkAddress: String; aNetWorkPort: UInt16; aTimeOut: Integer);
  end;

  TCPIPLisConnectionException = public class(LisConnectionException);
  
implementation

uses
  System.Threading;

constructor TCPIPLisConnection(aNetWorkAddress: String; aNetWorkPort: UInt16; aTimeOut: Integer);
begin
  inherited constructor;
  self.NetWorkAddress := aNetWorkAddress;
  self.NetWorkPort := aNetWorkPort;
  self.fReceiveTimeOutTimer.Interval := aTimeOut * 1000; //seconds to miliseconds
  self.fReceiveTimeOutTimer.Enabled := false;
  self.fReceiveTimeOutTimer.Elapsed += fReceiveTimeOutTimer_Elapsed;
  fLog := slf4net.LoggerFactory.GetLogger(typeOf(TCPIPLisConnection));
end;

constructor TCPIPLisConnection(aNetWorkAddress: String; aNetWorkPort: UInt16);
begin
  constructor(aNetWorkAddress, aNetWorkPort, 30); //30 seconds receive timeout (see 6.5.2.4 in LIS01-A2)
end;


method TCPIPLisConnection.SendMessage(aMessage: String);
begin
  fLog.Info(aMessage);
  fLog.Debug("send message");
  SendString(aMessage);
end;

method TCPIPLisConnection.Connect;
begin
  try
    if fIsInServerMode then exit;
    fSocket := new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); 
    if not fSocket.Connected then
    begin
      fLog.Info("Connecting To LIS...");
      fSocket.Connect(self.NetWorkAddress, self.NetWorkPort);
      fLog.Info("Connected");
      self.fTempReceiveBuffer.Clear;
      if fSocket.Connected then
      begin
        ReceiveLoop;
      end;
    end
    else
    begin
      fLog.Error('Could not connect to server because the connection is allready open');
      raise new TCPIPLisConnectionException('Could not connect to server because the connection is allready open');
    end;
  except
    on ex: Exception do 
    begin
      fLog.Error('Could not connect to server' + Environment.NewLine + ex.Message);
      raise new TCPIPLisConnectionException('Could not connect to server', ex);
    end;
  end;
end;

method TCPIPLisConnection.DisConnect;
begin
  self.fReceiveTimeOutTimer.Stop();
  fLog.Info("Disconnecting...");
  if fSocket.Connected then fSocket.Close;
  disposeAndNil(fSocket);
  fLog.Info("Disconnected");
  if assigned(self.OnLISConnectionClosed) then
  begin
    self.OnLISConnectionClosed.Invoke(self, new EventArgs);
  end;
end;

method TCPIPLisConnection.EstablishSendMode: Boolean;
begin
  //do nothing
  result := true;
end;

method TCPIPLisConnection.StopSendMode;
begin
  //do nothing
end;

method TCPIPLisConnection.ReceiveLoop;
begin
  try
    var buffer := new Byte[1024];  //1KByte buffer
    while fSocket:Connected do
    begin
      var ab := fSocket.Available;
      if ab > 0 then
      begin
        var ar := fSocket.Receive(buffer);
        var receivedString := Encoding.ASCII.GetString(buffer, 0, ar);
        for each i in receivedString do
        begin
          if i <> #0 then self.fTempReceiveBuffer.Append(i);
          if i = CR then
          begin
            if assigned(self.OnReceiveString) then
            begin
              fLog.Info("received: " + self.fTempReceiveBuffer.ToString());
              var arg := new LISConnectionReceivedDataEventArgs(self.fTempReceiveBuffer.ToString);
              self.OnReceiveString(self, arg); 
            end;
            self.fTempReceiveBuffer.Clear;
          end;
        end;
      end
      else
      begin
        Thread.Sleep(5);  //sleep 5 mS
      end;
    end;
  except
    on ex: SocketException where ex.ErrorCode = 10057 do
    begin
      fLog.Error(ex.Message); //Log the exception then exit to stop receiving.
    end;
  end;
end;

method TCPIPLisConnection.SendString(aString: String);
begin
  try
    var buffer :=  Encoding.UTF8.GetBytes(aString);
    fSocket.Send(buffer);
  except
    on ex: Exception do
    begin
      fLog.Error('Could not send data' + Environment.NewLine + ex.Message);
      raise new TCPIPLisConnectionException('Could not send data', ex);
    end;
  end;
end;

method TCPIPLisConnection.StartReceiveTimeoutTimer;
begin
  self.fReceiveTimeOutTimer.Start();
end;

method TCPIPLisConnection.Dispose;
begin
  var dis := IDisposable(fSocket);  
  dis:Dispose();
end;

method TCPIPLisConnection.fReceiveTimeOutTimer_Elapsed(sender: Object; e: System.Timers.ElapsedEventArgs);
begin
  self.fReceiveTimeOutTimer.Stop();
  if assigned(self.OnReceiveTimeOut) then
  begin
    self.OnReceiveTimeOut(self, new EventArgs);
  end;
end;

method TCPIPLisConnection.StartListeningAsync ;
begin
  fIsInServerMode := true;

  var localAddr := IPAddress.Parse(NetWorkAddress);
  var server := new TcpListener(localAddr,NetWorkPort);
  // Start listening for client Requests
  server.Start();
  // Buffer for reading data
  var bytes := new Byte[1024];
  var data : String := nil;

  // Start listening loop
  loop
  begin
    fClient:= server.AcceptTcpClient();
    fSocket := fClient.Client;
    data := nil;
    // Get a stream object for reading and writing
    var stream := fClient.GetStream();
    var i := stream.Read(bytes, 0, bytes.Length);

    // Loop to receive all the data sent by the client.
    while not (i  = 0) do
    begin
      // Translate data bytes to a ASCII string.
      data := System.Text.Encoding.ASCII.GetString(bytes, 0, i);
      //tbOutput.AppendText(data + Environment.NewLine);
      
      OnReceiveString:Invoke(self, new LISConnectionReceivedDataEventArgs(data));
      i := stream.Read(bytes, 0, bytes.Length);
    end;
    fClient.Close();
  end;
 
end;

method TCPIPLisConnection.StopListening(aTCPListener: TcpListener);
begin
  aTCPListener.Stop();
end;

end.