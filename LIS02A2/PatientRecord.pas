﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Text;

type
  
  PatientRecord = public class(AbstractLisRecord)
  public
    [LisRecordField(2)]
    property SequenceNumber: Int32;
    [LisRecordField(3)]
    property PracticeAssignedPatientID: String;
    [LisRecordField(4)]
    property LaboratoryAssignedPatientID: String;
    [LisRecordField(5)]
    property PatientID3: String;
    [LisRecordField(6)]
    property PatientName: PatientName;
    [LisRecordField(8)]
    [LisDateTimeUsage(LisDateTimeUsage.Date)]
    property Birthdate: nullable DateTime := nil;
    [LisRecordField(9)]
    property PatientSex: nullable PatientSex := nil;
    [LisRecordField(14)]
    property AttendingPhysicianID: String;
    method ToLISString: String; override;
  end;
  
implementation

method PatientRecord.ToLISString: String;
begin
  result := 'P' + LISDelimiters.FieldDelimiter 
    + inherited ToLISString;
end;

end.