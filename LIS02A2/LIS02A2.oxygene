<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <RootNamespace>Essy.LIS.LIS02A2</RootNamespace>
    <OutputType>Library</OutputType>
    <AssemblyName>E1394-91</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <Configuration Condition="'$(Configuration)' == ''">Release</Configuration>
    <TargetFrameworkVersion>v4.6</TargetFrameworkVersion>
    <ProjectGuid>{42A49954-6FE1-4200-96E5-924509051B8E}</ProjectGuid>
    <Company />
    <InternalAssemblyName />
    <StartupClass />
    <DefaultUses />
    <ApplicationIcon />
    <TargetFrameworkProfile />
    <ProjectView>ShowAllFiles</ProjectView>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <OutputPath>bin\Debug\</OutputPath>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>True</GenerateMDB>
    <CpuType>anycpu</CpuType>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <WarnOnCaseMismatch>True</WarnOnCaseMismatch>
    <RunCodeAnalysis>True</RunCodeAnalysis>
    <CheckForOverflowUnderflow>True</CheckForOverflowUnderflow>
    <RequireExplicitLocalInitialization>True</RequireExplicitLocalInitialization>
    <UseXmlDoc>True</UseXmlDoc>
    <CodeAnalysis>True</CodeAnalysis>
    <CodeAnalysisSet>default</CodeAnalysisSet>
    <CodeAnalysisSeverity>High</CodeAnalysisSeverity>
    <CodeAnalysisConfidence>Normal</CodeAnalysisConfidence>
    <Optimize>False</Optimize>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Release' ">
    <OutputPath>.\bin\Release</OutputPath>
    <EnableAsserts>False</EnableAsserts>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="mscorlib">
      <HintPath>mscorlib.dll</HintPath>
    </Reference>
    <Reference Include="slf4net">
      <HintPath>..\packages\slf4net.0.1.32.1\lib\net35\slf4net.dll</HintPath>
      <Private>True</Private>
    </Reference>
    <Reference Include="System">
      <HintPath>System.dll</HintPath>
    </Reference>
    <Reference Include="System.Xml" />
  </ItemGroup>
  <ItemGroup>
    <Compile Include="BaseRecord.pas" />
    <Compile Include="Enumerations.pas" />
    <Compile Include="Extensions.pas" />
    <Compile Include="HeaderRecord.pas" />
    <Compile Include="LisConnection.pas" />
    <Compile Include="LISLogConsole.pas" />
    <Compile Include="LISParser.pas" />
    <Compile Include="OrderRecord.pas" />
    <Compile Include="PatientRecord.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
    <Compile Include="QueryRecord.pas" />
    <Compile Include="ResultRecord.pas" />
    <Compile Include="SupportTypes.pas" />
    <Compile Include="TerminatorRecord.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <ItemGroup>
    <Content Include="..\Essy.LIS.LIS02A2.nuspec">
      <SubType>Content</SubType>
      <Link>Essy.LIS.LIS02A2.nuspec</Link>
    </Content>
    <Content Include="app.config">
      <SubType>Content</SubType>
    </Content>
    <Content Include="packages.config">
      <SubType>Content</SubType>
    </Content>
  </ItemGroup>
  <PropertyGroup>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'NugetBuild' ">
    <EnableAsserts>False</EnableAsserts>
    <OutputPath>bin\NugetBuild\</OutputPath>
  </PropertyGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.targets" />
  <PropertyGroup>
    <PreBuildEvent />
  </PropertyGroup>
</Project>