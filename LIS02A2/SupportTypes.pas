﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Reflection,
  System.Collections,
  System.Collections.Generic,
  System.Text;

type
  
  [AttributeUsage(AttributeTargets.Property)]
  LisRecordFieldAttribute = assembly class(Attribute)
  private
  public
    property FieldIndex: Int32;
    constructor(aFieldIndex: Int32);
  end;

  [AttributeUsage(AttributeTargets.Property)]
  LisRecordRemainingFieldsAttribute = assembly class(LisRecordFieldAttribute)
  private
  public
  end;

  [AttributeUsage(AttributeTargets.Field)]
  LisEnumAttribute = assembly class(Attribute)
  private
  public
    property LisID: String;
    constructor(aLisID: String);
  end;

  LisDateTimeUsage = assembly enum(DateTime, Date, Time);

  [AttributeUsage(AttributeTargets.Property)]
  LisDateTimeUsageAttribute = assembly class(Attribute)
  private
  public
    property DateTimeUsage: LisDateTimeUsage;
    constructor(aDateTimeUsage: LisDateTimeUsage);
  end;

  LisRecordType = public enum(Header, Patient, &Order, &Result, Comment, Query, Terminator, Scientific, Information);

  UniversalTestID = public class(AbstractLisSubRecord)
  public
    [LisRecordField(1)]
    property TestID: String;
    [LisRecordField(2)]
    property TestName: String;
    [LisRecordField(3)]
    property TestType: String;
    [LisRecordField(4)]
    property ManufacturerCode: String;
    [LisRecordRemainingFields(5)] //This will capture all fields above index 5 into this array
    property OptionalFields: array of String;
  end;

  PatientName = public class(AbstractLisSubRecord)
  public
    [LisRecordField(1)]
    property LastName: String;
    [LisRecordField(2)]
    property FirstName: String;
    [LisRecordField(3)]
    property MiddleName: String;
    [LisRecordField(4)]
    property Suffix: String;
    [LisRecordField(5)]
    property Title: String;
  end;

  StartingRange = public class(AbstractLisSubRecord)
  public
    [LisRecordField(1)]
    property PatientID: String;
    [LisRecordField(2)]
    property SpecimenID: String;
    [LisRecordField(3)]
    property Reserved: String;
  end;

  LISDelimiters = public static class
  private
  public
    EscapeCharacter: Char;
    FieldDelimiter: Char;
    ComponentDelimiter: Char;
    RepeatDelimiter: Char;
    class constructor;
    class method AddFieldDelimiters(numberOfDelimiters: Int32): String;
  end;

  RecordFields = assembly class
  private
    fItems: Array of String;
  public
    property Value: String;
    property Count: Integer read fItems.Length;
    constructor(lisString: String; aSeporatorChar: Char; aNumberOfFields: Int32);
    method GetField(indx: Int32): String;
  end;

implementation

uses 
  System.Diagnostics;

class constructor LISDelimiters;
begin
  FieldDelimiter := '|';
  RepeatDelimiter := '\';
  ComponentDelimiter := '^';
  EscapeCharacter := '&';
end;

class method LISDelimiters.AddFieldDelimiters(numberOfDelimiters: Int32): String;
begin
  result := new String(self.FieldDelimiter, numberOfDelimiters);
end;

constructor RecordFields(lisString: String; aSeporatorChar: Char; aNumberOfFields: Int32);
begin
  self.Value := lisString;
  self.fItems := lisString.Split([aSeporatorChar], aNumberOfFields);
end;

method RecordFields.GetField(indx: Int32): String;
begin
  if self.fItems.Length < indx then exit(String.Empty);
  result := self.fItems[indx - 1];
  //un-escape chars
  result := result.Replace(LISDelimiters.EscapeCharacter + 'F' + LISDelimiters.EscapeCharacter, LISDelimiters.FieldDelimiter);
  result := result.Replace(LISDelimiters.EscapeCharacter + 'S' + LISDelimiters.EscapeCharacter, LISDelimiters.ComponentDelimiter);
  result := result.Replace(LISDelimiters.EscapeCharacter + 'R' + LISDelimiters.EscapeCharacter, LISDelimiters.RepeatDelimiter);
  result := result.Replace(LISDelimiters.EscapeCharacter + 'E' + LISDelimiters.EscapeCharacter, LISDelimiters.EscapeCharacter);
end;

constructor LisRecordFieldAttribute(aFieldIndex: Int32);
begin
  self.FieldIndex := aFieldIndex;
end;

constructor LisEnumAttribute(aLisID: String);
begin
  self.LisID := aLisID;
end;

constructor LisDateTimeUsageAttribute(aDateTimeUsage: LisDateTimeUsage);
begin
  self.DateTimeUsage := aDateTimeUsage;
end;

end.