﻿namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Linq,
  System.Runtime.InteropServices,
  System.Text;

type
  LISLogConsole = public static class
  private
    const SW_HIDE: System.Int32 = 0;
    const SW_SHOW: System.Int32 = 5;
    const SC_CLOSE: LongWord = 61536;
    const MF_BYCOMMAND: LongWord = LongWord(0);
    [DllImport("kernel32.dll", SetLastError := true)]
    class method AllocConsole(): Boolean; external;
    [DllImport("kernel32.dll")]
    class method GetConsoleWindow(): IntPtr; external;
    [DllImport("user32.dll")]
    class method ShowWindow( hWnd: IntPtr; nCmdShow: Int32): Boolean; external; 
    [DllImport('Kernel32')]
    class method FreeConsole; external;
    [DllImport('user32.dll')]
    class method GetSystemMenu(hWnd: IntPtr; bRevert: Boolean): IntPtr; external;
    [DllImport('user32.dll')]
    class method DeleteMenu(hMenu: IntPtr; uPosition: LongWord; uFlags: LongWord): IntPtr; external;
  protected
  public
    class method ShowConsoleWindow;
    class method CloseConsoleWindow;
    
  end;

implementation

class method LISLogConsole.ShowConsoleWindow;
begin
  var handle := GetConsoleWindow();
  if handle = IntPtr.Zero then 
  begin
    AllocConsole();
    handle := GetConsoleWindow();
    var exitButton: IntPtr := GetSystemMenu(handle, false);
    if exitButton <> nil then DeleteMenu(exitButton, SC_CLOSE, MF_BYCOMMAND);
  end
  else 
  begin
    ShowWindow(handle, SW_SHOW);
  end
end;

class method LISLogConsole.CloseConsoleWindow;
begin
  var handle := GetConsoleWindow();
  if not (handle = IntPtr.Zero) then 
  begin
    FreeConsole();
  end;
end;

end.
