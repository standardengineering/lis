﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Text;

type
 
  HeaderRecord = public class(AbstractLisRecord)
  public
    [LisRecordField(3)]
    property MessageControlID: String;
    [LisRecordField(4)]
    property AccessPassword: String;
    [LisRecordField(5)]
    property SenderID: String;
    [LisRecordField(6)]
    property SenderStreetAddress: String;
    [LisRecordField(8)]
    property SenderTelephoneNumber: String;
    [LisRecordField(9)]
    property CharacteristicsOfSender: String;
    [LisRecordField(10)]
    property ReceiverID: String;
    [LisRecordField(11)]
    property Comment: String;
    [LisRecordField(12)]
    property ProcessingID: HeaderProcessingID;
    [LisRecordField(13)]
    property Version: String := 'LIS2-A2';  //specification version
    [LisRecordField(14)]
    property MessageDateTime: DateTime := DateTime.Now;
    method ToLISString: String; override;
    constructor(aLisString: String);
    constructor();
  end;
  
implementation

method HeaderRecord.ToLISString: String;
begin
  result := 'H' + LISDelimiters.FieldDelimiter 
  + LISDelimiters.RepeatDelimiter 
  + LISDelimiters.ComponentDelimiter 
  + LISDelimiters.EscapeCharacter 
  + LISDelimiters.FieldDelimiter 
  + inherited ToLISString;
end;

constructor HeaderRecord(aLisString: String);
begin
  LISDelimiters.FieldDelimiter := aLisString[1];
  LISDelimiters.RepeatDelimiter := aLisString[2];
  LISDelimiters.ComponentDelimiter := aLisString[3];
  LISDelimiters.EscapeCharacter := aLisString[4];
  inherited constructor(aLisString);
end;

constructor HeaderRecord();
begin

end;

end.