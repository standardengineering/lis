﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Text;

type
  
  HeaderProcessingID = public enum(None,
  [LisEnum('P')]
  Production, 
  [LisEnum('T')]
  Training, 
  [LisEnum('D')]
  Debugging, 
  [LisEnum('Q')]
  QualityControl);

  ResultAbnormalFlags = public enum(None, 
  [LisEnum('L')]
  BelowLowNormal, 
  [LisEnum('H')]
  AboveHighNormal,
  [LisEnum('LL')]
  BelowPanicNormal,
  [LisEnum('HH')]
  AbovePanicHigh, 
  [LisEnum('<')]
  BelowAbsolutelow, 
  [LisEnum('>')]
  AboveAbsoluteHigh, 
  [LisEnum('N')]
  Normal, 
  [LisEnum('A')]
  Abnormal, 
  [LisEnum('U')]
  SignificantChangeUp, 
  [LisEnum('D')]
  SignificantChangeDown, 
  [LisEnum('B')]
  Better, 
  [LisEnum('W')]
  Worse);

  ResultNatureOfAbnormalityTestingSet = public flags(None = 0, 
  [LisEnum('A')]
  Age, 
  [LisEnum('S')]
  Sex, 
  [LisEnum('R')]
  Race, 
  [LisEnum('N')]
  Normal);

  ResultStatus = public enum(None, 
  [LisEnum('C')]
  Correction, 
  [LisEnum('P')]
  PreliminaryResults, 
  [LisEnum('F')]
  FinalResults, 
  [LisEnum('X')]
  CannotBeDone, 
  [LisEnum('I')]
  ResultsPending, 
  [LisEnum('S')]
  PartialResults, 
  [LisEnum('M')]
  MICLevel,
  [LisEnum('R')]
  PreviouslyTransmitted, 
  [LisEnum('N')]
  NecessaryInformation, 
  [LisEnum('Q')]
  ResponseToOutstandingQuery, 
  [LisEnum('V')]
  ApprovedResult, 
  [LisEnum('W')]
  Warning);

  OrderPriority = public enum(None, 
  [LisEnum('S')]
  Stat,
  [LisEnum('A')]
  ASAP, 
  [LisEnum('R')]
  Routine,
  [LisEnum('C')]
  CallBack, 
  [LisEnum('P')]
  PreOperative);

  OrderActionCode = public enum(None, 
  [LisEnum('C')]
  Cancel,
  [LisEnum('A')]
  &Add, 
  [LisEnum('N')]
  &New, 
  [LisEnum('P')]
  Pending, 
  [LisEnum('L')]
  Reserved, 
  [LisEnum('X')]
  InProcess, 
  [LisEnum('Q')]
  QC);

  OrderReportType = public enum(None, 
  [LisEnum('O')]
  &Order, 
  [LisEnum('C')]
  Correction, 
  [LisEnum('P')]
  Preliminary,
  [LisEnum('F')]
  &Final, 
  [LisEnum('X')]
  Cancelled, 
  [LisEnum('I')]
  Pending, 
  [LisEnum('Y')]
  NoOrder, 
  [LisEnum('Z')]
  NoRecord, 
  [LisEnum('Q')]
  Response);

  TerminationCode = public enum(
  [LisEnum('N')]
  Normal,
  [LisEnum('T')]
  SenderAborted, 
  [LisEnum('R')]
  ReceiverRequestedAbort, 
  [LisEnum('E')]
  UnknownSystemError, 
  [LisEnum('Q')]
  ErrorInLastRequestForInformation, 
  [LisEnum('I')]
  NoInformationAvailableFromLastQuery, 
  [LisEnum('F')]
  LastRequestForInformationProcessed);

  PatientSex = public enum(
  [LisEnum('M')]
  Male,
  [LisEnum('F')]
  Female, 
  [LisEnum('U')]
  Unknown);
  
implementation

end.