﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Text,
  System.Threading,
  slf4net,
  Essy.LIS.Connection;

type
  LISParser = public class
  private
    fLog: ILogger;
    fConnection: ILisConnection;
    fOnReceivedRecord: ReceiveRecordEventHandler;
    fOnExceptionHappened: ThreadExceptionEventHandler;
    method Connection_OnReceiveTimeOut(sender: Object; e: EventArgs);
    method ParseReceivedRecord(aReceivedRecordString: String);
    method Set_Connection(value: ILisConnection);
    method ReceivedData(Sender: Object; e: LISConnectionReceivedDataEventArgs);
  protected
  public
//    class property LoggingEnabled:Boolean ;
    property Connection: ILisConnection read fConnection write set_Connection;
    property ThreadGuid: Guid;
    event OnReceivedRecord: ReceiveRecordEventHandler delegate fOnReceivedRecord;
    event OnExceptionHappened: ThreadExceptionEventHandler delegate fOnExceptionHappened;
    event OnSendProgress: EventHandler<SendProgressEventArgs>;
    constructor(aConnection: ILisConnection);
    method SendRecords(records: sequence of AbstractLisRecord);
  end;

  SendProgressEventArgs = public class(EventArgs)
  private
  public
    constructor(aProgress: Double;aThreadGuid: Guid);
    property Progress: Double;
    property ThreadGuid: Guid;
  end;

  ReceiveRecordEventArgs = public class(EventArgs)
  private
  public
    constructor(aReceivedRecord: AbstractLisRecord; aLisRecordType: LisRecordType);
    constructor;
    property ReceivedRecord: AbstractLisRecord;
    property RecordType: LisRecordType;
  end;

  ReceiveRecordEventHandler = public delegate(Sender: Object; e: ReceiveRecordEventArgs);

  LISParserException = public class(Exception);

  LISParserEstablishmentFailedException = public class(LISParserException);

  LISParserReceiveTimeOutException = public class(LISParserException);
  
implementation

uses 
  System.Collections;

constructor ReceiveRecordEventArgs(aReceivedRecord: AbstractLisRecord; aLisRecordType: LisRecordType);
begin
  inherited constructor;
  self.ReceivedRecord := aReceivedRecord;
  self.RecordType := aLisRecordType;
end;

constructor ReceiveRecordEventArgs;
begin
  inherited constructor;
end;

method LISParser.Set_Connection(value: ILisConnection);
begin
  if assigned(self.fConnection) then
  begin
    if self.fConnection <> value then
    begin
      self.fConnection.OnReceiveString -= ReceivedData;
      self.fConnection.OnReceiveTimeOut -= Connection_OnReceiveTimeOut;
    end;
  end;
  self.fConnection := value;
  self.fConnection.OnReceiveString += ReceivedData;
  self.fConnection.OnReceiveTimeOut += Connection_OnReceiveTimeOut;
end;

constructor LISParser(aConnection: ILisConnection);
begin
  self.Connection := aConnection;
  fLog := slf4net.LoggerFactory.GetLogger(typeOf(LISParser));
end;

method LISParser.ParseReceivedRecord(aReceivedRecordString: String);
begin
  fLog.Trace(aReceivedRecordString);
  var tempArgs := new ReceiveRecordEventArgs;
  var RecordTypeChar := aReceivedRecordString[0];
  case RecordTypeChar of
    'H':
      begin
        tempArgs.ReceivedRecord := new HeaderRecord(aReceivedRecordString);
        tempArgs.RecordType := LisRecordType.Header;
        self.fOnReceivedRecord(self, tempArgs);
      end;
    'P':
      begin
        tempArgs.ReceivedRecord := new PatientRecord(aReceivedRecordString);
        tempArgs.RecordType := LisRecordType.Patient;
        self.fOnReceivedRecord(self, tempArgs);  
      end;
    'O':
      begin
        tempArgs.ReceivedRecord := new OrderRecord(aReceivedRecordString);
        tempArgs.RecordType := LisRecordType.Order;
        self.fOnReceivedRecord(self, tempArgs);  
      end;
    'Q':
      begin
        tempArgs.ReceivedRecord := new QueryRecord(aReceivedRecordString);
        tempArgs.RecordType := LisRecordType.Query;
        self.fOnReceivedRecord(self, tempArgs); 
      end;
    'R':
      begin
        tempArgs.ReceivedRecord := new ResultRecord(aReceivedRecordString);
        tempArgs.RecordType := LisRecordType.Result;
        self.fOnReceivedRecord(self, tempArgs);  
      end;
    'L':
      begin
        tempArgs.ReceivedRecord := new TerminatorRecord(aReceivedRecordString);
        tempArgs.RecordType := LisRecordType.Terminator;
        self.fOnReceivedRecord(self, tempArgs);  
      end;
  end;
end;

method LISParser.ReceivedData(Sender: Object; e: LISConnectionReceivedDataEventArgs);
begin
  if not assigned(self.fOnReceivedRecord) then exit;
  try
    var records := e.ReceivedData.Split([#13]); //split incoming frame data into records by using CR as separator  
    for each r in records do
    begin
      if not String.IsNullOrEmpty(r) then ParseReceivedRecord(r);
    end;
  except
    on ex: Exception do 
    begin
      if assigned(self.fOnExceptionHappened) then
      begin
        fLog.Error(ex.Message);
        var ea := new ThreadExceptionEventArgs(ex);    
        self.fOnExceptionHappened(self, ea);
      end;
    end;
  end;
end;

method LISParser.SendRecords(records: sequence of AbstractLisRecord);
begin
  try
    if (records <> nil) then 
    begin
      if not self.Connection.EstablishSendMode then
        if not self.Connection.EstablishSendMode then
        begin
          fLog.Error('The LIS did not acknowledge the ENQ request.');
          raise new LISParserEstablishmentFailedException('The LIS did not acknowledge the ENQ request.');
        end;
      fLog.Info("Send Mode Established");
      var recCount: Double := 0;
      if records is IList then
      begin
        recCount := IList(records).Count;
      end;     
      for each rec in records index sendCounter do
      begin     
        //System.Diagnostics.Debug.WriteLine(ls);
        self.Connection.SendMessage(rec.ToLISString);
        if assigned(self.OnSendProgress) and (recCount > 0) then
        begin
          self.OnSendProgress(self, new SendProgressEventArgs(sendCounter / recCount, ThreadGuid));
        end;     
      end;
      self.Connection.StopSendMode;
    end;
  except
    on ex: Exception do 
    begin
      fLog.Error(ex, 'Exception in SendRecords');
      if assigned(self.fOnExceptionHappened) then
      begin
        var ea := new ThreadExceptionEventArgs(ex);    
        self.fOnExceptionHappened(self, ea);
      end;
    end;
  end;
end;

method LISParser.Connection_OnReceiveTimeOut(sender: Object; e: EventArgs);
begin
  if assigned(self.fOnExceptionHappened) then
  begin
    fLog.Error('No incoming data within timeout');
    var ea := new ThreadExceptionEventArgs(new LISParserReceiveTimeOutException('No incoming data within timeout'));    
    self.fOnExceptionHappened(self, ea);
  end;
end;

constructor SendProgressEventArgs(aProgress: Double;aThreadGuid: Guid);
begin
  self.Progress := aProgress;
  self.ThreadGuid := aThreadGuid;
end;

end.