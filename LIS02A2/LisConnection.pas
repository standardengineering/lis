﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.Connection;

interface

uses
  System.Collections.Generic,
  System.Text, 
  slf4net,
  System.Threading;

type

  /// <summary>
  /// This interface defines what is needed for the LIS02-A2 classes to communicate with outside world. (e.g. LIS01-A2)
  /// </summary>
  ILisConnection = public interface(IDisposable)
    /// <summary>
    /// Transmits a message to the receiver.
    /// </summary>
    /// <param name="aMessage">The message to be transmitted</param>   
    method SendMessage(aMessage: String); 
    /// <summary>
    /// Opens the connection, e.g. opens the COM Port.
    /// </summary>
    method Connect;
    /// <summary>
    /// Closes the connection, e.g. closes the COM port.
    /// </summary>
    method DisConnect; 
    /// <summary>
    /// Initiates the Establishment Phase if needed
    /// </summary>
    /// <returns>True if succesfull</returns>
    method EstablishSendMode: Boolean; 
    /// <summary>
    /// Sets the data link in a neutral state if needed.
    /// </summary>
    method StopSendMode; 
    /// <summary>
    /// Starts a receive timeout system if needed.
    /// </summary>
    method StartReceiveTimeoutTimer;
    /// <summary>
    /// This event is triggered when incoming message is received.
    /// </summary>
    event OnReceiveString: LISConnectionReceivedDataEventHandler;
    /// <summary>
    /// This event is fired when the connection is successfully closed.
    /// </summary>
    event OnLISConnectionClosed: EventHandler;
    /// <summary>
    /// This event fires when a receive timeout is detected.
    /// </summary>
    event OnReceiveTimeOut: EventHandler;
    /// <summary>
    /// The state of the connection
    /// </summary>
    /// <value>Idle, Sending, Receiving, Establishing</value>
    property Status: LisConnectionStatus read write;
  end;

  LISConnectionReceivedDataEventArgs = public class(EventArgs)
  private
  public
    constructor(aDataLine: String);
    property ReceivedData: String;
  end;
  
  LISConnectionReceivedDataEventHandler = public delegate(Sender: Object; e: LISConnectionReceivedDataEventArgs) ;

  LisConnectionStatus = public enum(Idle, Sending, Receiving, Establishing);

  LisConnectionException = public class(Exception);
  
implementation

constructor LISConnectionReceivedDataEventArgs(aDataLine: String);
begin
  self.ReceivedData := aDataLine;
end;

end.