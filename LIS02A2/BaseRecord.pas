﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Reflection,
  System.Collections.Generic,
  System.Text;

type
  
  AbstractLisRecord = public abstract class
  private
    method fCreateSubrecord(aString: String; aType: &Type): AbstractLisSubRecord;
    method fCreateLisEnum(aString: String; aType: &Type): Object;
    method fCreateRemainingFieldsArray(aRecordFields: RecordFields; aStartIndex: Integer): array of String;
    method fGetEnumLisString(aEnum: Object): String;
    method fEscapeString(aString: String; aSubrecord: Boolean): String;
    method fRemoveOptionalSubFields(aString: String): String;
  protected
    const CR = #13;   
  public
    method ToLISString: String; virtual;
    method ToString: String; override;
    constructor(aLisString: String);
    constructor();
  end;

  AbstractLisSubRecord = public abstract class(AbstractLisRecord)
  private
  public
  end;

implementation

constructor AbstractLisRecord(aLisString: String);
begin
  var isSubRecord := self is AbstractLisSubRecord;
  var sepChar := iif(isSubRecord, LISDelimiters.ComponentDelimiter, LISDelimiters.FieldDelimiter);
  var selfType := typeOf(self);
  var props := selfType.GetProperties();
  var limit := Int32.MaxValue;
  if (isSubRecord and (props.Length > 0)) then
  begin
    var lastPropIsArray := props[props.Length - 1].PropertyType.IsArray;
    if not lastPropIsArray then limit := props.Length;
  end;
  var rf := new RecordFields(aLisString, sepChar, limit);
  for each prop in props do
  begin
    var attribs := prop.GetCustomAttributes(typeOf(LisRecordFieldAttribute), false);
    if attribs.Length > 0 then
    begin
      var attrib := attribs[0] as LisRecordFieldAttribute;
      var field := rf.GetField(attrib.FieldIndex); 
      if not String.IsNullOrEmpty(field) then
      begin
        var propType := prop.PropertyType;
        var nullablePropType := &Nullable.GetUnderlyingType(propType);
        if assigned(nullablePropType) then propType := nullablePropType;
        case propType of
          typeOf(Int32): prop.SetValue(self, Int32.Parse(fRemoveOptionalSubFields(field)), nil);
          typeOf(String): prop.SetValue(self, field, nil);
          typeOf(DateTime): 
          begin
            var dateTimeUsage := LisDateTimeUsage.DateTime;
            attribs := prop.GetCustomAttributes(typeOf(LisDateTimeUsageAttribute), false);
            if attribs.Length = 1 then 
            begin
              var dtAttrib := attribs[0] as LisDateTimeUsageAttribute;
              dateTimeUsage := dtAttrib.DateTimeUsage;
            end;
            prop.SetValue(self, fRemoveOptionalSubFields(field).LisStringToDateTime(dateTimeUsage), nil);
          end;
        else
          if propType.IsEnum then
            prop.SetValue(self, fCreateLisEnum(fRemoveOptionalSubFields(field), propType), nil)
          else if propType.BaseType = typeOf(AbstractLisSubRecord) then
            prop.SetValue(self, fCreateSubrecord(field, propType), nil)
          else if propType.IsArray then
            if attrib is LisRecordRemainingFieldsAttribute then
            begin
              prop.SetValue(self, fCreateRemainingFieldsArray(rf, attrib.FieldIndex), nil);
            end
            else
              raise new FormatException('The LIS String was not of the correct format.');
        end;
      end;
    end;
  end;
end;

method AbstractLisRecord.ToLISString: String;
begin
  var isSubRecord := self is AbstractLisSubRecord;
  var sepChar := iif(isSubRecord, LISDelimiters.ComponentDelimiter, LISDelimiters.FieldDelimiter);
  var sb := new StringBuilder;
  var fieldList := new Dictionary<Int32, String>;
  var selfType := typeOf(self);
  var props := selfType.GetProperties();
  var maxFieldIndex := Int32.MinValue;
  var minFieldIndex := Int32.MaxValue;
  for each prop in props do
  begin
    var attribs := prop.GetCustomAttributes(typeOf(LisRecordFieldAttribute), false);
    if attribs.Length > 0 then
    begin
      var attrib := attribs[0] as LisRecordFieldAttribute;
      var propType := prop.PropertyType;
      var nullablePropType := &Nullable.GetUnderlyingType(propType);
      if assigned(nullablePropType) then propType := nullablePropType;
      var propString: String := nil;
      var propVal := prop.GetValue(self, nil);
      if assigned(propVal) then
      begin
        if propType = typeOf(DateTime) then
        begin
          var dateTimeUsage := LisDateTimeUsage.DateTime;
          attribs := prop.GetCustomAttributes(typeOf(LisDateTimeUsageAttribute), false);
          if attribs.Length = 1 then 
          begin
            var dtAttrib := attribs[0] as LisDateTimeUsageAttribute;
            dateTimeUsage := dtAttrib.DateTimeUsage;
          end;
          propString := DateTime(propVal).ToLISDate(dateTimeUsage);
        end
        else if propType.IsEnum then
          propString := fGetEnumLisString(propVal)
        else if propType.BaseType = typeOf(AbstractLisSubRecord) then
          propString := AbstractLisSubRecord(propVal).ToLISString
        else if propType.IsArray then
          begin
            if attrib is LisRecordRemainingFieldsAttribute then
            begin
              var ar := propVal as array of String;
              propString := String.Join(sepChar, ar);
            end;
          end
        else
          propString := propVal.ToString;
      end;
      if not String.IsNullOrEmpty(propString) then
      begin
        fieldList.Add(attrib.FieldIndex, propString);
        if attrib.FieldIndex > maxFieldIndex then maxFieldIndex := attrib.FieldIndex;   
      end;
      if attrib.FieldIndex < minFieldIndex then minFieldIndex := attrib.FieldIndex; 
    end;
  end;
  if minFieldIndex <= maxFieldIndex then
    for i: Int32 := minFieldIndex to maxFieldIndex - 1 do
    begin
      var field: String;
      fieldList.TryGetValue(i, out field);
      if not String.IsNullOrEmpty(field) then 
      begin
        sb.Append(fEscapeString(field, isSubRecord));
      end;
      sb.Append(sepChar);
    end;
  var field: String;
  fieldList.TryGetValue(maxFieldIndex, out field);
  if not String.IsNullOrEmpty(field) then sb.Append(field);
  if not isSubRecord then sb.Append(CR);
  result := sb.ToString();
end;

constructor AbstractLisRecord();
begin
end;

method AbstractLisRecord.fCreateSubrecord(aString: String; aType: &Type): AbstractLisSubRecord;
begin
  result := Activator.CreateInstance(aType, [aString]) as AbstractLisSubRecord;
end;

method AbstractLisRecord.fGetEnumLisString(aEnum: Object): String;
begin
  var enumType := typeOf(aEnum);
  var flagsAttribs := enumType.GetCustomAttributes(typeOf(FlagsAttribute), false);
  var isFlags := flagsAttribs.Length > 0;
  if isFlags then
  begin
    result := String.Empty;
    var inputVals := aEnum.ToString.Split(',');
    var enumValues := enumType.GetFields();
    for each ev in enumValues do
    begin
      var attribs: array of LisEnumAttribute := array of LisEnumAttribute (ev.GetCustomAttributes(typeOf(LisEnumAttribute), false));
      if attribs.Length > 0 then
      begin
        for each iv in inputVals do
        begin
          if iv.Trim() = ev.Name then result := result + attribs[0].LisID;
        end;
      end;
    end;
  end
  else
  begin
    var fi := enumType.GetField(aEnum.ToString());
    var attribs: array of LisEnumAttribute := array of LisEnumAttribute (fi.GetCustomAttributes(typeOf(LisEnumAttribute), false));
    exit iif(attribs.Length > 0, attribs[0].LisID, nil);
  end;
end;

method AbstractLisRecord.fCreateLisEnum(aString: String; aType: &Type): Object;
begin
  var flagsAttribs := aType.GetCustomAttributes(typeOf(FlagsAttribute), false);
  var isFlags := flagsAttribs.Length > 0;
  if isFlags then
  begin
    var inputString := String.Empty;
    var enumStringValue: String := nil;
    for each fi: FieldInfo in aType.GetFields() do 
    begin
      var attribs: array of LisEnumAttribute := array of LisEnumAttribute(fi.GetCustomAttributes(typeOf(LisEnumAttribute), false));
      if attribs.Length > 0 then enumStringValue := attribs[0].LisID;
      for each ch in aString do
      begin
        if System.String.Compare(enumStringValue, ch, true) = 0 then inputString := inputString + fi.Name + ',';
      end;
    end;
    if inputString.Length > 0 then 
    begin
      inputString := inputString.Remove(inputString.Length - 1, 1);
      exit &Enum.Parse(aType, inputString);
    end;
  end
  else
  begin
    var enumStringValue: String := nil;
    for each fi: FieldInfo in aType.GetFields() do 
    begin
      var attribs: array of LisEnumAttribute := array of LisEnumAttribute(fi.GetCustomAttributes(typeOf(LisEnumAttribute), false));
      if attribs.Length > 0 then enumStringValue := attribs[0].LisID;
      if System.String.Compare(enumStringValue, aString, true) = 0 then exit &Enum.Parse(aType, fi.Name);
    end;
  end;
end;

method AbstractLisRecord.ToString: String;
begin
  var sb := new StringBuilder;
  var selfType := typeOf(self);
  var props := selfType.GetProperties();
  for each prop in props do
  begin
    var attribs := prop.GetCustomAttributes(typeOf(LisRecordFieldAttribute), false);
    if attribs.Length > 0 then
    begin
      var propVal := prop.GetValue(self, nil);
      var propString: String := nil;
      if assigned(propVal) then propString := propVal.ToString;  
      if not String.IsNullOrEmpty(propString) then
      begin
        sb.Append(prop.Name);
        sb.Append(': ');
        sb.AppendLine(propString);
      end;
    end;
  end;
  result := sb.ToString();
end;

method AbstractLisRecord.fEscapeString(aString: String; aSubrecord: Boolean): String;
begin
  result := aString;
  result := result.Replace(LISDelimiters.EscapeCharacter, LISDelimiters.EscapeCharacter + 'E' + LISDelimiters.EscapeCharacter);
  result := result.Replace(LISDelimiters.FieldDelimiter, LISDelimiters.EscapeCharacter + 'F' + LISDelimiters.EscapeCharacter); 
  if aSubrecord then
  begin
    result := result.Replace(LISDelimiters.ComponentDelimiter, LISDelimiters.EscapeCharacter + 'S' + LISDelimiters.EscapeCharacter);
  end;
end;

method AbstractLisRecord.fRemoveOptionalSubFields(aString: String): String;
begin
  if aString.Contains(LISDelimiters.ComponentDelimiter) then
  begin
    exit aString.Split(LISDelimiters.ComponentDelimiter)[0];
  end
  else
  begin
    exit aString;
  end;
end;

method AbstractLisRecord.fCreateRemainingFieldsArray(aRecordFields: RecordFields; aStartIndex: Integer): array of String;
begin
  var temp := new List<String>; 
  for i: Integer := aStartIndex to aRecordFields.Count do
  begin
    temp.Add(aRecordFields.GetField(i));
  end;
  result := temp.ToArray();
end;

end.
