﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Text;

type
  
  OrderRecord = public class(AbstractLisRecord)
  public
    [LisRecordField(2)]
    property SequenceNumber: Integer;
    [LisRecordField(3)]
    property SpecimenID: String;
    [LisRecordField(5)]
    property TestID: UniversalTestID;
    [LisRecordField(6)]
    property Priority: OrderPriority;
    [LisRecordField(7)]
    [LisDateTimeUsage(LisDateTimeUsage.DateTime)]
    property RequestedDateTime: nullable DateTime;
    [LisRecordField(12)]
    property ActionCode: OrderActionCode;
    [LisRecordField(26)]
    property ReportType: OrderReportType;
    method ToLISString: String; override;
  end;
  
implementation

{ OrderRecord }

method OrderRecord.ToLISString: String;
begin
  result := 'O' + LISDelimiters.FieldDelimiter
    + inherited ToLISString();
end;

end.