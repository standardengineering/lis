﻿//Copyright (C) 2009 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.LIS.LIS02A2;

interface

uses
  System.Collections.Generic,
  System.Text,
  System.Globalization,
  System.Runtime.CompilerServices;

type
  
  [Extension]
  LISExtensions= assembly static class
  public
    [Extension]
    class method ToLISDate(aDateTime: DateTime; aLisDateTimeUsage: LisDateTimeUsage): String;
    [Extension]
    class method LisStringToDateTime(lisString: String; aLisDateTimeUsage: LisDateTimeUsage): DateTime;
  end;
  
implementation

class method LISExtensions.ToLISDate(aDateTime: DateTime; aLisDateTimeUsage: LisDateTimeUsage): String;
begin
  case aLisDateTimeUsage of
    LisDateTimeUsage.Date: 
      result := aDateTime.Year.ToString('D4') + 
                aDateTime.Month.ToString('D2') +
                aDateTime.Day.ToString('D2');
   
    LisDateTimeUsage.DateTime:  
      result := aDateTime.Year.ToString('D4') + 
                aDateTime.Month.ToString('D2') +
                aDateTime.Day.ToString('D2') +
                aDateTime.Hour.ToString('D2') + 
                aDateTime.Minute.ToString('D2') +
                aDateTime.Second.ToString('D2');
   
    LisDateTimeUsage.Time:   
      result := aDateTime.Hour.ToString('D2') + 
                aDateTime.Minute.ToString('D2') +
                aDateTime.Second.ToString('D2');
  end;
  
end;

class method LISExtensions.LisStringToDateTime(lisString: String; aLisDateTimeUsage: LisDateTimeUsage): DateTime;
begin
  case aLisDateTimeUsage of
    LisDateTimeUsage.Date: 
    begin;
      result := DateTime.ParseExact(lisString, 'yyyyMMdd', CultureInfo.InvariantCulture);
    end;
    LisDateTimeUsage.DateTime: 
    begin;
      result := DateTime.ParseExact(lisString, 'yyyyMMddHHmmss', CultureInfo.InvariantCulture);
    end;
    LisDateTimeUsage.Time: 
    begin;
      result := DateTime.ParseExact(lisString, 'HHmmss', CultureInfo.InvariantCulture);
    end;
  end;
 
end;


end.