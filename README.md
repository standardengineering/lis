# README #

This library is a .NET OO implementation of the CLSI LIS2-A2 standard.

"Specification for Transferring Information Between Clinical Laboratory Instruments and Information Systems; Approved Standard—Second Edition"

## How to use (C# example) ##

1: Make sure that your project is set to target .NET 4.6 (currently the NuGet packages only contain .NET 4.6 assemblies)

2: Get the packages from NuGet: 

* [CLSI LIS2-A2 High Level Objects](https://www.nuget.org/packages/Essy.LIS.LIS02A2/)
* [LIS01-A2 Connection Class ](https://www.nuget.org/packages/Essy.LIS.LIS01-A2/)

3: Add these namespaces to your code:
```csharp
using Essy.LIS.Connection;
using Essy.LIS.LIS02A2;
```

4: create a connection object, you can pick TCP/IP or a Serial Port.

* TCP/IP:
```csharp
      var someIP = "192.168.1.11";
      UInt16 somePort = 1111;
      var lowLevelConnection = new Lis01A02TCPConnection(someIP, somePort);
      var lisConnection = new Lis01A2Connection(lowLevelConnection);
```
* Serial Port:
```csharp
      var sp = new System.IO.Ports.SerialPort("COM1");
      // Config Serial port to your needs
      var lowLevelConnection = new Lis01A02RS232Connection(sp);
      var lisConnection = new Lis01A2Connection(lowLevelConnection);
```

5: Create LIS parser object and connect:
```csharp
      var LISParser = new LISParser(lisConnection);
      LISParser.OnSendProgress += LISParser_OnSendProgress; //Send data progress will trigger this event
      LISParser.OnReceivedRecord += LISParser_OnReceivedRecord; //incoming LIS frames will trigger this event
      LISParser.Connection.Connect();
```
```csharp
    private static void LISParser_OnReceivedRecord(object Sender, ReceiveRecordEventArgs e)
    {
      
    }

    private static void LISParser_OnSendProgress(object sender, SendProgressEventArgs e)
    {
      
    }
```

6: Now you are ready to receive incoming packets or you can transmit data:
```csharp
      var lisRecordList = new List<AbstractLisRecord>();
      var hr = new HeaderRecord();
      hr.SenderID = "Some Sender ID Code";
      hr.ProcessingID = HeaderProcessingID.Production;
      lisRecordList.Add(hr);
      var pr = new PatientRecord();
      pr.SequenceNumber = 1;
      pr.LaboratoryAssignedPatientID = "Sam001";
      lisRecordList.Add(pr);
      var orderRec = new OrderRecord();
      orderRec.SequenceNumber = 1;
      orderRec.SpecimenID = "Sam001";
      orderRec.TestID = new UniversalTestID();
      orderRec.TestID.ManufacturerCode = "T001";
      orderRec.ReportType = OrderReportType.Final;
      lisRecordList.Add(orderRec);
      pr = new PatientRecord();
      pr.SequenceNumber = 2;
      pr.LaboratoryAssignedPatientID = "Sam002";
      lisRecordList.Add(pr);
      orderRec = new OrderRecord();
      orderRec.SequenceNumber = 1;
      orderRec.SpecimenID = "Sam002";
      orderRec.TestID = new UniversalTestID();
      orderRec.TestID.ManufacturerCode = "T001";
      orderRec.ReportType = OrderReportType.Final;
      lisRecordList.Add(orderRec);
      var tr = new TerminatorRecord();
      lisRecordList.Add(tr);
      LISParser.SendRecords(lisRecordList); //transmit records to LIS server
```
You can fill the record data to your own needs. There are more record type that you can instantiate and transmit. The data is transmitted in the order of the list.